"""
Set of programs to read and interact with atom files focus.
"""

import os
import re
import pickle
import shutil
import datetime
import warnings
from io import StringIO

import numpy as np
import scipy.special as special
from astropy import units
from scipy import interpolate

# set defaults
## whitespace
whsp = "    "
## Default Voronov File
EBYSUS_ENVIRON = os.environ.get("EBYSUS", None)
if EBYSUS_ENVIRON is None:
    warnmsg = (
        "Warning, os.environ['EBYSUS'] has not been set. "
        + "Please use os.environ['EBYSUS'] = 'put/here/the/abspath/to/ebysus' to set it. "
        + "Or, set a different default value for DEFAULT_VORONOV_FILE in atom_tools.py..."
    )
    warnings.warn(warnmsg)
    EBYSUS_ENVIRON = ""  # set to a string so we don't crash due to next line.
DEFAULT_VORONOV_FILE = os.path.join(EBYSUS_ENVIRON, "INPUT/MISC/voronov.dat")


class Atom_tools(object):
    """
    Reads data from atom files in Ebysus format.

    Tools to calculate ionization/recombination fractions and
    populations.
    """

    def __init__(
        self,
        atom_file="",
        fdir="./",
        verbose=False,
        keywords=[
            "ar85-cdi",
            #'ar85-cea', #JMS think there is some bug, viggoh disagrees...
            "ar85-ch",  # not ready yet.
            "ar85-che",  # not ready yet.
            "ci",
            "ce",
            "cp",
            "ohm",
            "burgess",
            #'slups', # Not even started
            "shull82",
            "phhy",
            "reco",
        ],
    ):
        """
        Init file.

        Parameters (INPUT)
        ----------
        atom_file - atom file of interest (in this case, atom_name
                    and stage will be ignore).

        Parameters (DATA IN OBJECT)
        ----------
        params - list containing the information of the atom files
        """

        self.fdir = fdir
        self.verbose = verbose
        self.keyword_atomic = keywords
        # other option could be, for instance: ['voronov','mcwhirter']
        if atom_file != "":
            self.atom_file = atom_file
            self.params = AtomFile(atom_file, format="EBYSUS", fdir=self.fdir)
            self.atom = self.params.element  # ['atom']
            self.atom = self.atom.lower()

    def __repr__(self):
        """
        called when doing str(self); gives informative details about self.
        """
        return "<Atom_tools object with self.params={}. (object={})>".format(
            str(self.params), object.__repr__(self)
        )

    def get_nlevels(self):
        return self.params.nlevel

    def get_abund(self, Chianti=False, abundfile="sun_photospheric_1998_grevesse", voronov=False):
        """
        Returns abundances from the atom file or Chianti.

        Parameters
        ----------
        Chianti - if true uses chianti data base, otherwise uses atom files
                information
        """

        if Chianti:
            import ChiantiPy.core as ch

            if ".dat" in abundfile:
                abundfile = "sun_photospheric_1998_grevesse"
            self.ion = ch.ion(self.atom + "_" + str(1), 1e5, abundance=abundfile)
            return self.ion.Abundance
        elif voronov:
            if len(self.atom) > 0:
                if self.stage == 1:
                    stage = ""
                else:
                    stage = self.stage

                self.abund_dic = self.vor_params["SPECIES"][
                    [np.where(self.vor_params["SPECIES"][:, 1] == self.atom + str(stage))[0]],
                    8,
                ].astype(np.float)[0][0]
            else:
                for ii in range(0, self.vor_params["NLVLS_MAX"][0]):
                    if not (any(i.isdigit() for i in self.vor_params["SPECIES"][ii, 1])):
                        try:
                            abund_dic[self.vor_params["SPECIES"][ii, 1]] = self.vor_params["SPECIES"][
                                ii, 8
                            ].astype(np.float)
                        except BaseException:
                            abund_dic = {
                                self.vor_params["SPECIES"][ii, 1]: self.vor_params["SPECIES"][ii, 8].astype(
                                    np.float
                                )
                            }

                self.abund_dic = abund_dic
            return self.abund_dic
        else:
            return 10 ** (self.params.abund - 12)

    def get_atomweight(self):
        """
        Returns atomic weights from the atom file.

        Parameters
        ----------
        """
        return self.params.atomic_weight

    def get_atomde(self, level=1, mode="atomf", cm1=False):
        """
        Returns ionization energy dE from params file.

        Parameters
        ----------
        Chianti - if true uses chianti data base, otherwise uses atom files
                information
        cm1 - boolean and if it is true converts from eV to cm-1
        """
        if not hasattr(self, "cm1"):
            self.cm1 = cm1
        else:
            if self.cm1 != cm1:
                self.cm1 = cm1

        if self.cm1:
            units = 1.0 / (8.621738e-5 / 0.695)
        else:
            units = 1.0

        if mode.lower() == "chianti" and self.atom != "":
            import ChiantiPy.core as ch

            ion = ch.ion(self.atom + "_" + str(level))
            self.de = ion.Ip * units
            return self.de
        elif mode.lower() == "atomf":
            return self.params.levels[level - 1][0]
        elif mode.lower() == "voronov":
            return self.find_crosstype("voronov")[level]["data"]["dE"]

    def get_atomZ(self):
        """
        Returns atomic number Z from Chianti data base.

        Parameters
        ----------
        """
        import ChiantiPy.core as ch

        ion = ch.ion(self.atom + "_" + str(1), 1e5)
        self.Z = ion.Z
        return self.Z

    def get_voro_P(self, level):
        """
        Returns P parameter for Voronov rate fitting term from the atom file.
        The parameter P was included to better fit the particular cross-section
        behavior for certain ions near threshold; it only takes on the value 0
        or 1.

        Parameters
        ----------
        level - level transition.
        """

        return self.find_crosstype("voronov")[level]["data"]["P"]

    def get_voro_A(self, level):
        """
        Returns A parameter for Voronov rate fitting term from the atom file.

        Parameters
        ----------
        level - level transition.
        """

        return self.find_crosstype("voronov")[level]["data"]["A"]

    def get_voro_X(self, level):
        """
        Returns X parameter for Voronov rate fitting term from the atom file.

        Parameters
        ----------
        level - level transition.
        """

        return self.find_crosstype("voronov")[level]["data"]["X"]

    def get_voro_K(self, level):
        """
        Returns K parameter for Voronov rate fitting term  from the atom file.

        Parameters
        ----------
        level - level transition.
        """

        return self.find_crosstype("voronov")[level]["data"]["K"]

    def info_atom(self):
        """
        provides general information about specific atom, e.g., ionization and
        excitation levels etc.

        Parameters
        ----------
        atom - lower case letter of the atom of interest, e.g., 'he'
        """
        import ChiantiPy.core as ch

        ion = ch.ion(self.atom + "_" + str(1))
        Z = ion.Z
        wgt = self.get_atomweight(atom)
        print("Atom", "Z", "Weight  ", "FIP  ", "Abnd")
        print(self.atom, " ", Z, wgt, ion.FIP, ion.Abundance)
        for ilvl in range(1, Z + 1):
            ion = ch.ion(atom + "_" + str(ilvl))
            print("    ", "ion", "Level", "dE (eV)", "g")
            print("    ", ion.Spectroscopic, ion.Ion, " ", ion.Ip, "g")
            if hasattr(ion, "Elvlc"):
                nl = len(ion.Elvlc["lvl"])
                print("        ", "Level", "str    ", "dE (cm-1)", "g")
                for elvl in range(0, nl):
                    print(
                        "          ",
                        ion.Elvlc["lvl"][elvl],
                        ion.Elvlc["pretty"][elvl],
                        ion.Elvlc["ecmth"][elvl],
                        "g",
                    )

    def get_excidE(self, stage=1, cm1=False):
        """
        Returns ionization energy dE for excited levels.

        Parameters
        ----------
        params - list containing the information of voronov.dat (following
            the format of read_voro_file)
        atom - lower case letter if the atom of interest.
            If atom is not defined then it returns a list of all ionization
            energy dE in the file.
            In this case, one will access to it by, e.g., var['he2']
        cm1 - boolean and if it is true converts from eV to cm-1
        """
        import ChiantiPy.core as ch

        if not hasattr(self, cm1):
            self.cm1 = cm1
        else:
            if self.cm1 != cm1:
                self.cm1 = cm1

        unitscm1 = 1.0 / (8.621738e-5 / 0.695)
        if self.cm1:
            units = 1.0
        else:
            units = 1.0 / unitscm1

        ion = ch.ion(self.atom + "_" + str(stage))
        if hasattr(ion, "Elvlc"):
            self.de = (ion.Ip * unitscm1 + ion.Elvlc["ecmth"][stage]) * units
        else:
            print("No Elvlc in the Chianti Data base")

    def rrec_mcwhirter(self, Te, lvl_str=1, lvl_end=2):
        """
        This is an equation from 'Spectral Intensities' by  McWhirter, R. W. P.
        and it is valid only for H and neutral plasma (n_e .eq. n_i)

        [LaTeX] $<\sigma_{rec} \nu_{e}> = 0.7 \times 10^{-19}
            \sqrt{\frac{13.6}{T_e}}\,[m^3/s]$ // [Note: sqrt(13.6) * 0.7 = 2.6]
        frec = 0.7e-19 * 1e6 * sqrt(scr1) ! 1e6 to move from m^3/s to
        cm^3/s JMS this version is only for H.
        """
        from helita.sim.bifrost import Bifrost_units

        units = Bifrost_units(verbose=self.verbose)
        TeV = Te * units.k_to_ev
        vfac = 0.7e-19
        self.stage = 1
        self.get_atomZ()
        dE = float(self.params.levels[lvl_end - 1][0]) - float(self.params.levels[lvl_str - 1][0])
        dEeV = dE * 8065.6
        return vfac * np.sqrt(dEeV / TeV) * self.Z**2 * 1e6

    def rrec_shull82(self, Te, lvl_str=1, lvl_end=2):
        """
        Recombination rate coefficients Shull and Steenberg (1982)
        for the recombination rate combines the sum of radiative and dielectronic recombination rate
        alpha_r = Arad (T_4)^(-Xrad) ; and alpha_d = Adi T^(-3/2) exp(-T0/T) (1+Bdi exp(-T1/T))
        i  j   Acol     Tcol     Arad     Xrad      Adi      Bdi       T0       T1
        """
        tr_line = (self.find_trns_crosstype("shull82", lvl_str=lvl_str))[0]
        self.find_crosstype("shull82")[tr_line]["data"][0]
        self.find_crosstype("shull82")[tr_line]["data"][1]
        Arad = self.find_crosstype("shull82")[tr_line]["data"][2]
        Xrad = self.find_crosstype("shull82")[tr_line]["data"][3]
        Adi = self.find_crosstype("shull82")[tr_line]["data"][4]
        Bdi = self.find_crosstype("shull82")[tr_line]["data"][5]
        T0 = self.find_crosstype("shull82")[tr_line]["data"][6]
        T1 = self.find_crosstype("shull82")[tr_line]["data"][7]
        summrs = 1.0
        T_4 = Te / 1e4
        arrec = Arad * T_4 ** (-Xrad)
        adrec = Adi / Te / np.sqrt(Te) * np.exp(-T0 / Te) * (1.0 + Bdi * np.exp(-T1 / Te))
        return arrec + summrs * adrec

    def rrec_ce(self, Te, lvl_str=1, lvl_end=2):
        tr_line = (self.find_trns_crosstype("ce", lvl_str=lvl_str))[0]
        ct = self.read_ct(Te, tr_line, "ce")
        # nel removed since it will be added in vion

        g_ilv = float(self.params.levels[lvl_str - 1][1])
        g_jlv = float(self.params.levels[lvl_end - 1][1])

        return ct * g_ilv * np.sqrt(Te) / g_jlv

    def rrec_reco(self, Te, lvl_str=1, lvl_end=2):
        tr_line = (self.find_trns_crosstype("reco", lvl_str=lvl_end))[0]
        return self.read_ct(Te, tr_line, "reco")

    def rrec_ohm(self, Te, lvl_str=1, lvl_end=2):
        # JMS I am not sure if the first one is the high one. it may need to be change from lvl_end to lvl_str
        tr_line = (self.find_trns_crosstype("ohm", lvl_str=lvl_str))[0]
        ct = self.read_ct(Te, tr_line, "ohm")
        float(self.params.levels[lvl_str - 1][1])
        g_jlv = float(self.params.levels[lvl_end - 1][1])
        # nel removed since it will be added in vion
        return 8.63e-6 * ct / g_jlv / np.sqrt(Te)

    def rrec(self, ntot, Te, lvl_str=1, lvl_end=2, gencol_key="voronov", threebody=False):
        """
        gives the recombination rate per particle
        Parameters:
        ------
        threebody  - False or the gencol_key for ionization.
        """

        if threebody:
            if hasattr(self, "cup"):
                self.cdn = self.r3body(ntot, Te, lvl_str=lvl_str, lvl_end=lvl_end, gencol_key=gencol_key)
            else:
                self.cdn = 0
        else:
            self.cdn = 0

        if gencol_key.lower() == "atomic":
            keylist = self.keyword_atomic
        elif gencol_key.lower() == "voronov":
            keylist = "mcwhirter"
        else:
            keylist = gencol_key.lower()
        if np.size(keylist) == 1:
            keylist = [keylist]

        for keyword in keylist:
            if (keyword == "mcwhirter") and (len(self.find_crosstype("voronov")) > 0):  # mcwhirter65
                self.cdn += self.rrec_mcwhirter(Te, lvl_str, lvl_end)

            elif (keyword == "shull82") and (len(self.find_crosstype(keyword)) > 0):
                self.cdn += self.rrec_shull82(Te, lvl_str, lvl_end)

            elif (keyword == "ce") and (len(self.find_crosstype(keyword)) > 0):
                self.cdn += self.rrec_ce(Te, lvl_str, lvl_end)

            elif (keyword == "reco") and (len(self.find_crosstype(keyword)) > 0):
                self.frec = self.rrec_reco(Te, lvl_str, lvl_end)

            elif (keyword == "ohm") and (len(self.find_crosstype(keyword)) > 0):
                self.cdn += self.rrec_ohm(Te, lvl_str, lvl_end)

    def vrec(self, nel, Te, lvl_str=1, lvl_end=2, gencol_key="voronov", threebody=False):
        """
        gives the recombination frequency.
        """
        # if not hasattr(self, 'cdn'):
        self.rrec(
            nel,
            Te,
            lvl_str=lvl_str,
            lvl_end=lvl_end,
            gencol_key=gencol_key,
            threebody=threebody,
        )
        if not hasattr(self, "frec"):
            self.frec = 0.0
        if hasattr(self, "cdn"):
            self.frec += nel * self.cdn
        else:
            self.cdn += self.frec / nel

    def find_crosstype(self, crosstype):
        crospos = [
            num for num, dic in enumerate(self.params.collision_tables) if dic["type"] == crosstype.upper()
        ]
        if crospos == []:
            if self.verbose:
                print("No cross keyword %s" % crosstype)
            return crospos
        else:
            output = []
            if self.verbose:
                print("Cross keyword %s" % crosstype)
            for trns in crospos:
                output = np.append(output, self.params.collision_tables[trns])
            return output

    def find_trns_crosstype(self, crosstype, lvl_str=None, lvl_end=None):
        cross = self.find_crosstype(crosstype)
        if lvl_str is None:
            return [v for v, bla in enumerate(cross) if bla["level_end"] == lvl_end]
        else:
            return [v for v, bla in enumerate(cross) if bla["level_start"] == lvl_str]

    def read_ct(self, Te, tr_line, gencol_key):
        temp_tab = self.find_crosstype(gencol_key)[tr_line]["temp"]
        ct_tab = self.find_crosstype(gencol_key)[tr_line]["data"]
        finterp = interpolate.interp1d(temp_tab, ct_tab, fill_value="extrapolate")
        return finterp(Te)

    def ar85cea(self, i_lo, i_hi, te):
        """
        new routine for computing collisional autoionization rates using
        formalism and formulae from arnaud and rothenflug 1985 ar85cea 94-02-22
        new routine: (philip judge) 19-10-11  atom_tools version (jms)
        """
        from helita.sim.bifrost import Bifrost_units

        cup = 0.0
        cea1 = np.zeros(np.size(te))
        cea2 = np.zeros(np.size(te))
        debug_ar85 = True
        units = Bifrost_units(verbose=self.verbose)
        # find element
        iz = self.get_atomZ()
        zz = 0.0 + iz
        if (iz < 1) or (iz > 92):
            print("ar85-cea:  atomic number = %i for %d" % (iz, self.atom))
            print("ar85-cea:  no autoionization included")
            cea1 = np.reshape(cea1, np.shape(te))
            cea2 = np.reshape(cea2, np.shape(te))
            return cea1, cea2
        elif debug_ar85:
            print("ar85-cea: %s charge %i " % (self.atom, self.params.levels[i_lo - 1][-1]))

        # find iso-electronic sequence
        ichrge = self.params.levels[i_lo - 1][-1] - 1
        isoseq = iz - ichrge
        cseq = atomnm(isoseq - 1)

        for k, tei in enumerate(np.ravel(te)):
            bkt = units.k_to_ev * tei
            #! lithium sequence
            if cseq == "li":
                ceab = 1.0 / (1.0 + 2.0e-4 * zz * zz * zz)
                ceazeff = zz - 1.3
                ceaiea = 13.6 * ((zz - 0.835) * (zz - 0.835) - 0.25 * (zz - 1.62) * (zz - 1.62))
                y = ceaiea / bkt
                dy = -y / bkt * units.k_to_ev
                f1y = fone(y, 0)
                df1y = fone(y, 1) * dy
                #  preprint  cup= 8.0e+10 * ceab /ceazeff/ceazeff/sqrt(bkt)
                #  preprint   *   * exp(-y)*(2.22*f1y+0.67*(1.-y*f1y)+0.49*y*f1y+1.2*(y-f1y))
                fac = 2.22 * f1y + 0.67 * (1.0 - y * f1y) + 0.49 * y * f1y + 1.2 * y * (1.0 - y * f1y)

                cup = 1.60e-07 * 1.2 * ceab / ceazeff / ceazeff / np.sqrt(bkt) * np.exp(-y) * fac
                dcupdt = (
                    -0.5 * cup / bkt * units.k_to_ev
                    - cup * dy
                    + cup
                    / fac
                    * (
                        2.22 * df1y
                        - 0.67 * (f1y * dy + y * df1y)
                        + 0.49 * (f1y * dy + df1y * y)
                        + 1.2 * (dy * (1.0 - y * f1y) - y * (dy * f1y + y * df1y))
                    )
                )

                # two special cases:
                # c iv - app a ar85
                if self.atom.lower() == "c":
                    cup = cup * 0.6
                    dcupdt = dcupdt * 0.6

                # n v  - app a ar85
                elif self.atom == "n":
                    cup = cup * 0.8
                    dcupdt = dcupdt * 0.8
                # o vi - app a ar85
                elif self.atom == "o":
                    cup = cup * 1.25
                    dcupdt = dcupdt * 1.25

            # sodium sequence
            elif cseq == "na":
                if iz <= 16:
                    ceaa = 2.8e-17 * (zz - 11.0) ** (-0.7)
                    ceaiea = 26.0 * (zz - 10.0)
                    y = ceaiea / bkt
                    f1y = fone(y, 0)
                    dy = -y / bkt * units.k_to_ev
                    df1y = fone(y, 1) * dy

                    cup = 6.69e7 * ceaa * ceaiea / np.sqrt(bkt) * np.exp(-y) * (1.0 - y * f1y)
                    dcupdt = (
                        -0.5 * cup / bkt * units.k_to_ev
                        - cup * dy
                        - cup / (1.0 - y * f1y) * (dy * f1y - y * df1y)
                    )
                elif (iz >= 18) and (iz <= 28):
                    ceaa = 1.3e-14 * (zz - 10.0) ** (-3.73)
                    ceaiea = 11.0 * (zz - 10.0) * np.sqrt(zz - 10.0)
                    y = ceaiea / bkt
                    f1y = fone(y, 0)
                    dy = -y / bkt * units.k_to_ev
                    df1y = fone(y, 1) * dy
                    fac = 1.0 - 0.5 * (y - y * y + y * y * y * f1y)

                    cup = 6.69e7 * ceaa * ceaiea / np.sqrt(bkt) * np.exp(-y) * fac
                    if fac != 0.0:
                        dcupdt = (
                            -0.5 * cup / bkt * units.k_to_ev
                            - cup * dy
                            - 0.5
                            * cup
                            / fac
                            * (dy - 2.0 * y * dy + 3.0 * y * y * dy * f1y + y * y * y * df1y)
                        )
                    else:
                        dcupdt = 0.0
                else:
                    cea1 = np.reshape(cea1, np.shape(te))
                    cea2 = np.reshape(cea2, np.shape(te))
                    return cea1, cea2

            # magnesium-sulfur sequences
            elif (cseq == "mg") or (cseq == "al") or (cseq == "si") or (cseq == "p") or (cseq == "s"):
                if cseq == "mg":
                    ceaiea = 10.3 * (zz - 10.0) ** 1.52

                if cseq == "al":
                    ceaiea = 18.0 * (zz - 11.0) ** 1.33

                if cseq == "si":
                    ceaiea = 18.4 * (zz - 12.0) ** 1.36

                if cseq == "p":
                    ceaiea = 23.7 * (zz - 13.0) ** 1.29

                if cseq == "s":
                    ceaiea = 40.1 * (zz - 14.0) ** 1.1

                ceaa = 4.0e-13 / zz / zz / ceaiea
                y = ceaiea / bkt
                f1y = fone(y, 0)
                dy = -y / bkt * units.k_to_ev
                df1y = fone(y, 1) * dy
                fac = 1.0 - 0.5 * (y - y * y + y * y * y * f1y)

                cup = 6.69e7 * ceaa * ceaiea / np.sqrt(bkt) * np.exp(-y) * fac
                if cup != 0.0:
                    dcupdt = (
                        -0.5 * cup / bkt * units.k_to_ev
                        - cup * dy
                        - 0.5 * cup / fac * (dy - 2.0 * y * dy + 3.0 * y * y * dy * f1y + y * y * y * df1y)
                    )
                else:
                    dcupdt = 0.0

            #  special cases
            if (self.atom == "ca") and (ichrge == 0):
                ceaa = 9.8e-17
                ceaiea = 25.0
                ceab = 1.12
                y = ceaiea / bkt
                f1y = fone(y, 0)
                dy = -y / bkt * units.k_to_ev
                df1y = fone(y, 1) * dy
                fac = 1.0 + ceab * f1y

                cup = 6.69e7 * ceaa * ceaiea / np.sqrt(bkt) * np.exp(-y) * fac
                dcupdt = -0.5 * cup / bkt * units.k_to_ev - cup * dy - 0.5 * cup / fac * ceab * df1y
                print("ar85-cea special case %s ion ichrge %i" % (self.atom, ichrge))

            elif (self.atom == "ca") and (ichrge == 1):
                ceaa = 6.0e-17
                ceaiea = 25.0
                ceab = 1.12
                y = ceaiea / bkt
                f1y = fone(y, 0)
                dy = -y / bkt * units.k_to_ev
                df1y = fone(y, 1) * dy
                fac = 1.0 + ceab * f1y

                cup = 6.69e7 * ceaa * ceaiea / np.sqrt(bkt) * np.exp(-y) * fac
                dcupdt = -0.5 * cup / bkt * units.k_to_ev - cup * dy - 0.5 * cup / fac * ceab * df1y

            elif (self.atom == "fe") and (ichrge == 3):
                ceaa = 1.8e-17
                ceaiea = 60.0
                ceab = 1.0
                y = ceaiea / bkt
                f1y = fone(y, 0)
                dy = -y / bkt * units.k_to_ev
                df1y = fone(y, 1) * dy
                fac = 1.0 + ceab * f1y

                cup = 6.69e7 * ceaa * ceaiea / np.sqrt(bkt) * np.exp(-y) * fac
                dcupdt = -0.5 * cup / bkt * units.k_to_ev - cup * dy - 0.5 * cup / fac * ceab * df1y

            elif (self.atom == "fe") and (ichrge == 4):
                ceaa = 5.0e-17
                ceaiea = 73.0
                ceab = 1.0
                y = ceaiea / bkt
                f1y = fone(y, 0)
                dy = -y / bkt * units.k_to_ev
                df1y = fone(y, 1) * dy
                fac = 1.0 + ceab * f1y

                cup = 6.69e7 * ceaa * ceaiea / np.sqrt(bkt) * np.exp(-y) * fac
                dcupdt = -0.5 * cup / bkt * units.k_to_ev - cup * dy - 0.5 * cup / fac * ceab * df1y

            cea1[k] = cup
            cea2[k] = dcupdt
        cea1 = np.reshape(cea1, np.shape(te))
        cea2 = np.reshape(cea2, np.shape(te))
        return cea1, cea2

    def rion_voronov(self, Te, lvl_str, lvl_end):
        """
        Ionization rates from  S. Voronov "A Practical Fit Formula for
        Ioinization Rate Coef. of Aatoms and Iions by Electron Impact: Z A
        1-28" [1997]

        [LaTeX] $\nu^{ion}= n_e A \frac{1+\sqrt{\frac{\phi_{ion}}{T^*_e}} P}{X+\phi_{ion}/T^*_e}
                            (\frac{\phi_{ion}}{T^*_e})^K e^{-\phi_{ion}/T^*_e} [s^{-1}]$
        n_e         : electron density number in cgs units [g/cm^3]
        Z           : atomic number
        T^{*}_{e}   : electron temperature in [eV]
        \phi_{ion}. : treshold energy [eV]
        P {0 or 1}  : parameter for better fit
        A, K, X     : adjustable parameters
        """
        from helita.sim.bifrost import Bifrost_units

        tr_line = self.find_trns_crosstype("voronov", lvl_str=lvl_str)
        # get_atomde(atom, Chianti=False)  # 13.6
        phion = self.get_atomde(level=tr_line[0], mode="voronov")
        units = Bifrost_units(verbose=self.verbose)
        TeV = Te * units.k_to_ev

        # get_atomA(atom)
        A = self.get_voro_A(tr_line[0])  # * 1e6
        # get_atomX(atom)  # 0.232
        X = self.get_voro_X(tr_line[0])
        # get_atomK(atom)  # 0.39
        K = self.get_voro_K(tr_line[0])
        # get_atomP(atom)  # 0
        P = self.get_voro_P(tr_line[0])
        return (
            A * (1 + np.sqrt(phion / TeV) * P) / (X + phion / TeV) * (phion / TeV) ** K * np.exp(-phion / TeV)
        )

    def rion_ar85cdi(self, Te, lvl_str, lvl_end):
        """
        Data for electron impact ionization Arnaud and Rothenflug (1985)

        1/(u I^2) (A (1 - 1/u) + B (1 - 1/u)^2) + C ln(u) +
                                                 D ln(u)/u) (cm^2)
             #   i   j
             # Numbers of shells
         # dE(eV)  A   B   C   D
        """
        from helita.sim.bifrost import Bifrost_units

        units = Bifrost_units(verbose=self.verbose)
        TeV = Te * units.k_to_ev

        tr_line = (self.find_trns_crosstype("ar85-cdi", lvl_str=lvl_str))[0]
        nshells = self.find_crosstype("ar85-cdi")[tr_line]["nshells"]
        phion = np.zeros(nshells)
        A = np.zeros(nshells)
        B = np.zeros(nshells)
        C = np.zeros(nshells)
        D = np.zeros(nshells)
        for ishell in range(0, nshells):
            phion[ishell] = self.find_crosstype("ar85-cdi")[tr_line]["data"][ishell, 0]
            A[ishell] = self.find_crosstype("ar85-cdi")[tr_line]["data"][ishell, 1]
            B[ishell] = self.find_crosstype("ar85-cdi")[tr_line]["data"][ishell, 2]
            C[ishell] = self.find_crosstype("ar85-cdi")[tr_line]["data"][ishell, 3]
            D[ishell] = self.find_crosstype("ar85-cdi")[tr_line]["data"][ishell, 4]
        cup = np.zeros(np.shape(TeV))
        for ishell in range(0, nshells):
            dE = phion[ishell] * units.ev_to_erg
            xj = dE / units.kboltzmann / Te
            fac = np.exp(-xj) * np.sqrt(xj)
            fone_out = np.zeros(np.size(xj))
            ftwo_out = np.zeros(np.size(xj))
            for i, ixj in enumerate(xj.reshape(np.size(xj))):
                fone_out[i] = fone(ixj, 0)
                ftwo_out[i] = ftwo(ixj, 0)
            fone_out = np.reshape(fone_out, np.shape(xj))
            ftwo_out = np.reshape(ftwo_out, np.shape(xj))
            fxj = fac * (
                A[ishell]
                + B[ishell] * (1.0 + xj)
                + (C[ishell] - xj * (A[ishell] + B[ishell] * (2.0 + xj))) * fone_out
                + D[ishell] * xj * ftwo_out
            )
            fac = 6.69e-07 / phion[ishell] / np.sqrt(phion[ishell])
            cup += fac * fxj
        return cup

    def rion_ar85cea(self, Te, lvl_str, lvl_end):
        tr_line = (self.find_trns_crosstype("ar85-cea", lvl_str=lvl_str))[0]
        coeff = self.find_crosstype("ar85-cea")[tr_line]["data"][0]
        # var1 contains cup rate, var2 contains d(cup)/dtemp
        var1, var2 = self.ar85cea(lvl_str, lvl_end, Te)
        return var1 * coeff

    def rion_burgess(self, Te, lvl_str, lvl_end):
        tr_line = (self.find_trns_crosstype("burgess", lvl_str=lvl_str))[0]
        coeff = self.find_crosstype("burgess")[tr_line]["data"][0]

        zc = self.params.levels[lvl_str - 1][-1]  # stage AKA ichrge
        betab = 0.25 * (np.sqrt((100.0 * zc + 91) / (4.0 * zc + 3)) - 5.0)
        cbar = 2.3

        dEeV = float(self.params.levels[lvl_end - 1][0]) - float(
            self.params.levels[lvl_str - 1][0]
        )  # / 8065.6
        dE = dEeV * units.clight * units.hplanck

        scr1 = dE / Te / units.kboltzmann

        dekti = 1.0 / scr1
        wlog = np.log(1.0 + dekti)
        wb = wlog ** (betab / (1.0 + dekti))
        exin1 = float(special.expn(1, scr1))
        qb = 2.1715e-8 * cbar * (13.6 / dEeV) ** 1.5 * np.sqrt(Te) * exin1 * wb
        # nel removed since it will be added in vion V
        return qb * coeff

    def rion_shull82(self, Te, lvl_str, lvl_end):
        """
        Recombination rate coefficients Shull and Steenberg (1982)
        provides direct collisional ionization with the following
        fitting:
        Ci  = Acol T^(0.5) (1 + Ai T / Tcol)^(-1) exp(-Tcol/T),
            with Ai ~ 0.1
        for the recombination rate combines the sum of radiative
        and dielectronic recombination rate
        alpha_r = Arad (T_4)^(-Xrad) ; and alpha_d = Adi T^(-3/2)
        i  j   Acol     Tcol     Arad     Xrad      Adi      Bdi
            exp(-T0/T) (1+Bdi exp(-T1/T))
                T0       T1
        """
        Te / 1e4
        tr_line = (self.find_trns_crosstype("shull82", lvl_str=lvl_str))[0]
        Acol = self.find_crosstype("shull82")[tr_line]["data"][0]
        Tcol = self.find_crosstype("shull82")[tr_line]["data"][1]
        self.find_crosstype("shull82")[tr_line]["data"][2]
        self.find_crosstype("shull82")[tr_line]["data"][3]
        self.find_crosstype("shull82")[tr_line]["data"][4]
        self.find_crosstype("shull82")[tr_line]["data"][5]
        self.find_crosstype("shull82")[tr_line]["data"][6]
        self.find_crosstype("shull82")[tr_line]["data"][7]
        # if any(T_4 < T0) or any(T_4 > T1):
        #    print('Warning[shull82]: Temperature out of bounds')

        cup = 0.0
        Ai = 0.1

        if Acol != 0.0:
            cup = Acol * np.sqrt(Te) * np.exp(-Tcol / Te) / (1.0e0 + Ai * Te / Tcol)

        return cup

    def rion_phhy(self, Te, lvl_str, lvl_end, tau=None):
        """
        Photoionization rate for hydrogen like atoms
        Ci = 8.*pi*alpha*Gaunc*nc*nu^3/c^2*expint1(dE/k_B/Trad)
        assumes J_nu = 2*h_Planck*nu^3/c^2*exp(-h_Planck/k_B/Trad)
        and alpha_nu = alpha_0*(nu_ion_edge/nu)^3; see
        Vernazza, Avrett, Loeser 1981, ApJS, vol 45, p. 662
        Note that the rate is reduced by a factor exp(-h_Planck*nu/k_B/Te)
        where h_Planck*nu is the energy from the ground state to the state
        photoionization is active: for hydrogen like atoms (on the Sun)
        photoionization occurs mainly from the first excited state. So,
        this formula is not correct for He I as it is NOT hydrogen like...
        but it should not be too horrible as a starting point. Trad should
        be replaced with Te at high opacity (in photosphere and below),
        but if this is important then you should be solving the full
        radiative transport equation.
        """
        from helita.sim.bifrost import Bifrost_units

        units = Bifrost_units(verbose=self.verbose)
        tr_line = (self.find_trns_crosstype("phhy", lvl_str=lvl_str))[0]
        nc = self.find_crosstype("phhy")[tr_line]["data"][0]
        almb = self.find_crosstype("phhy")[tr_line]["data"][1]
        alfa = self.find_crosstype("phhy")[tr_line]["data"][2]
        gaunc = self.find_crosstype("phhy")[tr_line]["data"][3]
        Trad = self.find_crosstype("phhy")[tr_line]["data"][4]

        dE = float(self.params.levels[lvl_end - 1][0]) - float(self.params.levels[lvl_str - 1][0])
        dE_ground = 1.0 / almb / 1.0e-8  # AA_2_cm
        dE = dE - dE_ground
        dE = dE * units.clight * units.hplanck
        dE_ground = dE_ground * units.clight * units.hplanck

        vc = dE / units.hplanck
        fac = 8.0 * np.pi * alfa * gaunc * nc * vc / units.clight * vc / units.clight * vc
        cup = (
            np.exp(-dE_ground / units.kboltzmann / Te)
            * fac
            * float(special.expn(1, dE / units.kboltzmann / Trad))
            * np.exp(-tau)
        )

        return cup

    def rion_ar85ch(self, Te, lvl_str, lvl_end, keyword="ar85-ch"):
        """
        charge transfer recombination with neutral hydrogen Arnaud
        and Rothenflug (1985) updated for Fe ions by Arnaud and
        Rothenflug (1992)
        alpha = a (T_4)^b (1 + c exp(d T_4))
        i   j
        Temperature range (K)   a(1e-9cm3/s)    b      c    d
        """
        tr_line = (self.find_trns_crosstype(keyword, lvl_str=lvl_end))[0]
        T_4 = Te / 1e4
        self.find_crosstype(keyword)[tr_line]["data"][0]
        self.find_crosstype(keyword)[tr_line]["data"][1]
        a = self.find_crosstype(keyword)[tr_line]["data"][2]
        b = self.find_crosstype(keyword)[tr_line]["data"][3]
        c = self.find_crosstype(keyword)[tr_line]["data"][4]
        d = self.find_crosstype(keyword)[tr_line]["data"][5]
        # if any(T_4 < T0) or any(T_4 > T1):### Checi fi this one is correct
        #    print('Warning[%s]: Temperature out of bounds' % keyword)
        return a * (T_4) ** b * (1.0 + c * np.exp(d * T_4))

    def rion_ci(self, Te, lvl_str, lvl_end):
        from helita.sim.bifrost import Bifrost_units

        tr_line = (self.find_trns_crosstype("ci", lvl_end=lvl_end))[0]
        ct = self.read_ct(Te, tr_line, "ci")
        units = Bifrost_units(verbose=self.verbose)
        # JMS is this in eV or not? The output looks ok. # VHH it is in cm^{-1} correct conversion to energy (h*\nu) below
        dE = float(self.params.levels[lvl_end - 1][0]) - float(self.params.levels[lvl_str - 1][0])
        dE = dE * units.clight * units.hplanck

        scr1 = dE / Te / units.kboltzmann

        # nel removed since it will be added in vion
        return ct * np.exp(-scr1) * np.sqrt(Te)

    def rion_ce(self, Te, lvl_str, lvl_end):
        from helita.sim.bifrost import Bifrost_units

        tr_line = (self.find_trns_crosstype("ce", lvl_str=lvl_str))[0]
        ct = self.read_ct(Te, tr_line, "ce")
        units = Bifrost_units(verbose=self.verbose)

        dE = float(self.params.levels[lvl_end - 1][0]) - float(self.params.levels[lvl_str - 1][0])
        # JMS is this in eV or not?
        dE = dE * units.clight * units.hplanck
        g_ilv = float(self.params.levels[lvl_str - 1][1])
        g_jlv = float(self.params.levels[lvl_end - 1][1])
        scr1 = dE / Te / units.kboltzmann
        bltz = g_jlv / g_ilv * np.exp(-scr1)

        # nel removed since it will be added in vion
        cdn = ct * g_ilv * np.sqrt(Te) / g_jlv
        return bltz * cdn

    def rion_ohm(self, Te, lvl_str, lvl_end):
        # JMS I am not sure if the first one is the high one. it may need to be change from lvl_end to lvl_str
        tr_line = (self.find_trns_crosstype("ohm", lvl_str=lvl_str))[0]
        ct = self.read_ct(Te, tr_line, "ohm")
        g_ilv = float(self.params.levels[lvl_str - 1][1])
        g_jlv = float(self.params.levels[lvl_end - 1][1])
        dE = float(self.params.levels[lvl_end - 1][0]) - float(self.params.levels[lvl_str - 1][0])
        dE = dE * units.clight * units.hplanck

        scr1 = dE / Te / units.kboltzmann
        bltz = g_jlv / g_ilv * np.exp(-scr1)
        # nel removed since it will be added in vion
        cdn = 8.63e-6 * ct / g_jlv / np.sqrt(Te)
        return bltz * cdn

    def rion(self, Te, lvl_str=1, lvl_end=2, gencol_key="voronov", tau=0.0):
        """
        gives the ionization rate per particle using Voronov 1997 fitting
        formula.
        """

        if gencol_key.lower() == "atomic":
            keylist = self.keyword_atomic
        else:
            keylist = [gencol_key.lower()]

        self.cup = 0.0

        for keyword in keylist:
            if keyword == "voronov":
                self.cup += self.rion_voronov(Te, lvl_str, lvl_end)

            elif (keyword == "ar85-cdi") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_ar85cdi(Te, lvl_str, lvl_end)

            elif (keyword == "ar85-cea") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_ar85cea(Te, lvl_str, lvl_end)

            elif (keyword == "burgess") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_burgess(Te, lvl_str, lvl_end)

            elif (keyword == "shull82") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_shull82(Te, lvl_str, lvl_end)

            elif (keyword == "phhy") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_phhy(Te, lvl_str, lvl_end, tau=tau)

            elif (keyword == "ar85-ch") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_ar85ch(Te, lvl_str, lvl_end)

            elif (keyword == "ar85-che") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_ar85ch(Te, lvl_str, lvl_end, keyword)

            elif (keyword == "ci") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_ci(Te, lvl_str, lvl_end)

            elif (keyword == "ce") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_ci(Te, lvl_str, lvl_end)

            elif (keyword == "ohm") and (len(self.find_crosstype(keyword)) > 0):
                self.cup += self.rion_ohm(Te, lvl_str, lvl_end)

    def vion(self, nel, Te, lvl_str=1, lvl_end=2, gencol_key="voronov", tau=0.0):
        """
        gives the ionization frequency using Voronov 1997 fitting formula.
        """
        # if not hasattr(self, 'cup'):
        self.rion(Te, lvl_str=lvl_str, lvl_end=lvl_end, gencol_key=gencol_key, tau=tau)
        if not hasattr(self, "fion"):
            self.fion = 0.0
        if hasattr(self, "cup"):
            self.fion += nel * self.cup
        else:
            self.cup += self.fion / nel

    def ionfraction(self, ntot, Te, lvl_str=1, lvl_end=2, gencol_key="voronov"):
        """
        gives the ionization fraction using vrec and vion.
        """
        # if not hasattr(self, 'cup'):
        self.rion(Te, lvl_str=lvl_str, lvl_end=lvl_end, gencol_key=gencol_key)
        # if not hasattr(self, 'cdn'):
        self.rrec(ntot, Te, lvl_str=lvl_str, lvl_end=lvl_end, gencol_key=gencol_key)

        self.ionfrac = self.fion / (self.rec + 2.0 * self.fion)

    def ionse(self, ntot, Te, lvl_str=1, lvl_end=2, gencol_key="voronov"):
        """
        gives electron or ion number density using vrec and vion.
        """
        # if not hasattr(self, ionfrac):
        self.ionfraction(ntot, Te, lvl_str=lvl_str, lvl_end=lvl_end, gencol_key=gencol_key)
        self.ion_ndens = ntot * self.ionfrac

    def neuse(self, ntot, Te, lvl_str=1, lvl_end=2, gencol_key="voronov"):
        """
        gives neutral number density using vrec and vion.
        """
        # if not hasattr(self, ionfrac):
        self.ionse(Te, lvl_str=lvl_str, lvl_end=lvl_end, gencol_key=gencol_key)
        self.neu_ndens = ntot - 2.0 * self.ion_ndens

    def r3body(self, nel, Te, lvl_str=1, lvl_end=2, gencol_key="voronov"):
        """
        three body recombination.
        """
        from helita.sim.bifrost import Bifrost_units

        if not hasattr(self, "cup"):
            self.vion(nel, Te, lvl_str=lvl_str, lvl_end=lvl_end, gencol_key=gencol_key)

        units = Bifrost_units(verbose=self.verbose)
        gst_hi = float(self.params.levels[lvl_str - 1][1])  # 2.0
        gst_lo = float(self.params.levels[lvl_end - 1][1])  # 1.0

        """
        if lvl_str > 0:
            self.stage = lvl_str + 1
        else:
            self.stage = ''
        dekt = self.get_atomde(Chianti=False)
        dekt = float(self.get_atomde(Chianti=False))/ units.K_TO_EV / Te
        """
        dekt = float(self.params.levels[lvl_end - 1][0]) - float(self.params.levels[lvl_str - 1][0])
        dekt = dekt * units.clight * units.hplanck / Te / units.kboltzmann

        # Assuming nel in cgs. (For SI units would be 2.07e-22)
        # nel is added latter in rrec and others.
        dekt = np.ma.array(dekt, mask=dekt > 80.0, fill_value=80.0)
        saha = 2.07e-16 * gst_lo / gst_hi * Te ** (-1.5) * np.exp(dekt.filled())

        return saha * self.cup  # vion is collisional ionization rate

    def inv_pop_atomf(
        self,
        ntot,
        Te,
        niter=100,
        nel=None,
        threebody=True,
        gencol_key="voronov",
        tau=None,
    ):
        """
        Inverts the Matrix for Statistical Equilibrum.
        """
        # nel starting guess is:
        if nel is None:
            nel = ntot * 1.0
        shape = np.shape(ntot)
        nelf = np.ravel(nel)
        ntotf = np.ravel(ntot)
        tef = np.ravel(Te)
        npoints = len(tef)
        if np.any(tau == None):
            tau = np.zeros(npoints)
        tauf = np.ravel(tau)
        nlevels = self.params.nlevel
        n_isp = np.zeros((npoints, nlevels))
        # percent=0.0
        for ipoint in range(0, npoints):
            # if (ipoint * 100 / (1.0 * npoints) >= percent):
            if ipoint % 100 == 0:
                print(
                    "Done %s grid points of %s" % (str(ipoint), str(npoints)),
                    end="\r",
                    flush=True,
                )
                # percent=percent+1.
            for iel in range(1, niter):
                B = np.zeros((nlevels))
                A = np.zeros((nlevels, nlevels))
                igen = 0
                for ilev in range(1, nlevels):
                    if hasattr(self, "cdn"):
                        delattr(self, "cdn")
                    if hasattr(self, "cup"):
                        delattr(self, "cup")
                    if hasattr(self, "fion"):
                        delattr(self, "fion")
                    if hasattr(self, "frec"):
                        delattr(self, "frec")
                    self.vion(
                        nelf[ipoint],
                        tef[ipoint],
                        lvl_str=ilev,
                        lvl_end=ilev + 1,
                        gencol_key=gencol_key,
                        tau=tauf[ipoint],
                    )
                    self.vrec(
                        nelf[ipoint],
                        tef[ipoint],
                        lvl_str=ilev,
                        lvl_end=ilev + 1,
                        threebody=threebody,
                        gencol_key=gencol_key,
                    )
                    # print(nelf[ipoint],tef[ipoint],self.cdn,self.cup)
                    if igen == 0:  # JMS Not sure what is that for....
                        Rip = self.frec
                    else:
                        Rip += self.frec
                    Cip = self.fion
                    A[ilev - 1, ilev - 1] += -Cip
                    A[ilev - 1, ilev] = Rip  # + Ri3d #Ri3d!?
                    if ilev < nlevels - 2:
                        A[ilev, ilev] = -Rip  # - Ri3d
                        A[ilev, ilev - 1] = Cip
                    igen = 1
                A[ilev, :] = 1.0
                B[ilev] = ntotf[ipoint]
                n_isp[ipoint, :] = np.linalg.solve(A, B)
                nelpos = 0.0
                for ilev in range(1, nlevels):
                    nelpos += n_isp[ipoint, ilev] * ilev
                if (nelf[ipoint] - nelpos) / (nelf[ipoint] + nelpos) < 1.0e-4:
                    # print("Jump iter with iter = ",iel)
                    nelf[ipoint] = nelpos
                    break
                if iel == niter - 1:
                    if (nelf[ipoint] - nelpos) / (nelf[ipoint] + nelpos) > 1.0e-4:
                        print(
                            "Warning, No stationary solution was found",
                            (nelf[ipoint] - nelpos) / (nelf[ipoint] + nelpos),
                            nelpos,
                            nelf[ipoint],
                            whsp * 5,
                            end="\r",
                            flush=True,
                        )
                nelf[ipoint] = nelpos
        self.n_el = np.reshape(nelf, (shape))
        self.n_isp = np.reshape(n_isp, (np.append(shape, nlevels)))


class Voronov_tools(object):
    """
    Reads data from vorovon file.
    """

    def __init__(self, atom_name="", stage="", voronov_file=DEFAULT_VORONOV_FILE):
        """
        Init file.

        Parameters (INPUT)
        ----------
        atom_name - lower case letter if the atom of interest.
                    This is mostly just for using some CHIANTI read out.
                    If atom is not defined then it returns a list of all
                    abundances in the file.
            In this case, one will access to it by, e.g., var['h'],
        stage - This is for the CHIANTI read out options.
        voronov_file - path of the Ebysus file that contains all the
                    information about Voronov. For CHIANTI stuff, it is
                    the abundance file, default is voronov.dat without
                    Chianti data base or sun_photospheric_1998_grevesse for
                    Chianti data base.

        Parameters (DATA IN OBJECT)
        ----------
        params - list containing the information of voronov.dat
                (following the format of read_voro_file)
        """
        self.voronov_file = voronov_file
        self.read_voro_file()
        if atom_name != "":
            self.atom = atom_name.replace("_", "")

            if len("".join(x for x in atom_name if x.isdigit())) == 1:
                self.stage = int(re.findall("\d+", atom_name)[0])
                self.atom = self.atom.replace("1", "")

            if stage is None:
                self.stage = 1
            else:
                self.stage = stage

    def read_voro_file(self):
        """
        Reads the miscelaneous Vofonov & abundances table formatted (command
        style) ascii file into dictionary.
        """
        li = 0
        self.vor_params = {}
        headers = ["NSPECIES_MAX", "NLVLS_MAX", "SPECIES"]
        # go through the file, add stuff to dictionary
        with open(self.voronov_file) as fp:
            for line in fp:
                # ignore empty lines and comments
                line = line.strip()
                if len(line) < 1:
                    li += 1
                    continue
                if line[0] == "#":
                    li += 1
                    continue
                line = line.split(";")[0].split("\t")

                if np.size(line) == 1:
                    if str(line[0]) in headers:
                        key = line
                    else:
                        value = line[0].strip()
                        try:
                            value = int(value)
                            exec('params["' + key[0] + '"] = [value]')
                        except BaseException:
                            print(
                                "(WWW) read_voro_file: could not find datatype" "in line %i, skipping" % li,
                                whsp * 5,
                                end="\r",
                                flush=True,
                            )
                            li += 1
                            continue
                elif np.size(line) > 8:
                    val_arr = []
                    for iv in range(0, 9):
                        if (iv == 0) or (iv == 4):
                            try:
                                value = int(line[iv].strip())
                                val_arr.append(value)
                            except BaseException:
                                print(
                                    "(WWW) read_voro_file: could not find"
                                    "datatype in line %i, skipping" % li,
                                    whsp * 5,
                                    end="\r",
                                    flush=True,
                                )
                                li += 1
                                continue
                        elif iv == 1:
                            val_arr.append(line[iv].strip().lower())
                        else:
                            try:
                                value = float(line[iv].strip())
                                val_arr.append(value)

                            except BaseException:
                                print(
                                    "(WWW) read_voro_file: could not find"
                                    "datatype in line %i, skipping" % li,
                                    whsp * 5,
                                    end="\r",
                                    flush=True,
                                )
                                li += 1
                                continue

                    if not key[0] in self.vor_params:
                        self.vor_params[key[0]] = [val_arr]
                    else:
                        self.vor_params[key[0]].append(val_arr)

                else:
                    print(
                        "(WWW) read_voro_file: could not find datatype in " "line %i, skipping" % li,
                        whsp * 5,
                        end="\r",
                        flush=True,
                    )
                    li += 1
                    continue

            self.vor_params["SPECIES"] = np.array(self.vor_params["SPECIES"])

    def get_atomweight(self):
        """
        Returns atomic weights from the voronov.dat file.
        """
        if len(self.atom) > 0:
            if self.stage == 1:
                stage = ""
            else:
                stage = self.stage

            self.weight_dic = self.vor_params["SPECIES"][
                [np.where(self.vor_params["SPECIES"][:, 1] == self.atom + str(stage))[0]],
                2,
            ].astype(np.float)[0][0]
        else:
            for ii in range(0, self.vor_params["NLVLS_MAX"][0]):
                if not (any(i.isdigit() for i in self.vor_params["SPECIES"][ii, 1])):
                    try:
                        weight_dic[self.vor_params["SPECIES"][ii, 1]] = self.vor_params["SPECIES"][
                            ii, 2
                        ].astype(np.float)
                    except BaseException:
                        weight_dic = {
                            self.vor_params["SPECIES"][ii, 1]: self.vor_params["SPECIES"][ii, 2].astype(
                                np.float
                            )
                        }

            self.weight_dic = weight_dic
        return self.weight_dic

    def get_atomde(self, Chianti=True, cm1=False):
        """
        Returns ionization energy dE from the voronov.dat file.

        Parameters
        ----------
        Chianti - if true uses chianti data base, otherwise uses atom files
                information
        cm1 - boolean and if it is true converts from eV to cm-1
        """
        if not hasattr(self, "cm1"):
            self.cm1 = cm1
        else:
            if self.cm1 != cm1:
                self.cm1 = cm1

        if not hasattr(self, "Chianti"):
            self.Chianti = Chianti
        else:
            if self.Chianti != Chianti:
                self.Chianti = Chianti

        if self.cm1:
            units = 1.0 / (8.621738e-5 / 0.695)
        else:
            units = 1.0

        if Chianti and self.atom != "":
            import ChiantiPy.core as ch

            ion = ch.ion(self.atom + "_" + str(self.stage))
            self.de = ion.Ip * units
            return self.de
        else:

            if len(self.atom) > 0:
                if self.stage == 1:
                    stage = ""
                else:
                    stage = self.stage
                self.de = (
                    self.vor_params["SPECIES"][
                        [np.where(self.vor_params["SPECIES"][:, 1] == self.atom + str(stage))[0]],
                        3,
                    ].astype(np.float)[0][0]
                    * units
                )
                return self.de
            else:
                for ii in range(0, self.vor_params["NLVLS_MAX"][0]):
                    try:
                        de_dic[self.vor_params["SPECIES"][ii, 1]] = (
                            self.vor_params["SPECIES"][ii, 3].astype(np.float) * units
                        )
                    except BaseException:
                        de_dic = {
                            self.vor_params["SPECIES"][ii, 1]: self.vor_params["SPECIES"][ii, 3].astype(
                                np.float
                            )
                            * units
                        }

                self.de_dic = de_dic
                return self.de_dic

    def get_atomZ(self, Chianti=True):
        """
        Returns atomic number Z from the voronov.dat file.

        Parameters
        ----------
                If atom is not defined then it returns a list of all Z
                parameter in the file. In this case, one will access to it by,
                e.g., var['he2']
        """

        if Chianti:
            import ChiantiPy.core as ch

            ion = ch.ion(self.atom + "_" + str(self.stage), 1e5)
            self.ion = ion
            self.Z = ion.Z
        else:
            if len(self.atom) > 0:
                if self.stage == 1:
                    stage = ""
                else:
                    stage = self.stage
                self.z = self.vor_params["SPECIES"][
                    [np.where(self.vor_params["SPECIES"][:, 1] == self.atom + str(stage))[0]],
                    0,
                ].astype(np.int)[0][0]
            else:
                for ii in range(0, self.vor_params["NLVLS_MAX"][0]):
                    if not (any(i.isdigit() for i in self.vor_params["SPECIES"][ii, 1])):
                        try:
                            z_dic[self.vor_params["SPECIES"][ii, 1]] = self.vor_params["SPECIES"][
                                ii, 0
                            ].astype(np.int)
                        except BaseException:
                            z_dic = {
                                self.vor_params["SPECIES"][ii, 1]: self.vor_params["SPECIES"][ii, 0].astype(
                                    np.int
                                )
                            }

                self.z_dic = z_dic

    def get_atomP(self):
        """
        Returns P parameter for Voronov rate fitting term from the voronov.dat
        file. The parameter P was included to better fit the particular cross-
        section behavior for certain ions near threshold; it only takes on the
        value 0 or 1.

        Parameters
        ----------
                If atom is not defined then it returns a list of all P
                parameter in the file. In this case, one will access to it by,
                e.g., var['he2']
        """

        if len(self.atom) > 0:
            if self.stage == 1:
                stage = ""
            else:
                stage = self.stage
            self.p = self.vor_params["SPECIES"][
                [np.where(self.vor_params["SPECIES"][:, 1] == self.atom + str(stage))[0]],
                4,
            ].astype(np.int)[0][0]
        else:
            for ii in range(0, self.vor_params["NLVLS_MAX"][0]):
                try:
                    p_dic[self.vor_params["SPECIES"][ii, 1]] = self.vor_params["SPECIES"][ii, 4].astype(
                        np.int
                    )
                except BaseException:
                    p_dic = {
                        self.vor_params["SPECIES"][ii, 1]: self.vor_params["SPECIES"][ii, 4].astype(np.int)
                    }

            self.p_dic = p_dic

    def get_atomA(self):
        """
        Returns A parameter for Voronov rate fitting term from the voronov.dat
        file.
        """

        if len(self.atom) > 0:
            if self.stage == 1:
                stage = ""
            else:
                stage = self.stage
            self.a = self.vor_params["SPECIES"][
                [np.where(self.vor_params["SPECIES"][:, 1] == self.atom + str(stage))[0]],
                5,
            ].astype(np.float)[0][0]
        else:
            for ii in range(0, self.vor_params["NLVLS_MAX"][0]):
                try:
                    a_dic[self.vor_params["SPECIES"][ii, 1]] = self.vor_params["SPECIES"][ii, 5].astype(
                        np.float
                    )
                except BaseException:
                    a_dic = {
                        self.vor_params["SPECIES"][ii, 1]: self.vor_params["SPECIES"][ii, 5].astype(np.float)
                    }

            self.a_dic = a_dic

    def get_atomX(self):
        """
        Returns X parameter for Voronov rate fitting term from the voronov.dat
        file.
        """

        if len(self.atom) > 0:
            if self.stage == 1:
                stage = ""
            else:
                stage = self.stage
            self.x = self.vor_params["SPECIES"][
                [np.where(self.vor_params["SPECIES"][:, 1] == self.atom + str(stage))[0]],
                6,
            ].astype(np.float)[0][0]
        else:
            for ii in range(0, self.vor_params["NLVLS_MAX"][0]):
                try:
                    x_dic[self.vor_params["SPECIES"][ii, 1]] = self.vor_params["SPECIES"][ii, 6].astype(
                        np.float
                    )
                except BaseException:
                    x_dic = {
                        self.vor_params["SPECIES"][ii, 1]: self.vor_params["SPECIES"][ii, 6].astype(np.float)
                    }

            self.x_dic = x_dic

    def get_atomK(self):
        """
        Returns K parameter for Voronov rate fitting term  from the voronov.dat
        file.
        """

        if len(self.atom) > 0:
            if self.stage == 1:
                stage = ""
            else:
                stage = self.stage
            self.k = self.vor_params["SPECIES"][
                [np.where(self.vor_params["SPECIES"][:, 1] == self.atom + str(stage))[0]],
                7,
            ].astype(np.float)[0][0]
        else:
            for ii in range(0, self.vor_params["NLVLS_MAX"][0]):
                try:
                    k_dic[self.vor_params["SPECIES"][ii, 1]] = self.vor_params["SPECIES"][ii, 7].astype(
                        np.float
                    )
                except BaseException:
                    k_dic = {
                        self.vor_params["SPECIES"][ii, 1]: self.vor_params["SPECIES"][ii, 7].astype(np.float)
                    }

            self.k_dic = k_dic


class AtomFile(object):
    """
    Class to hold data from an EBYSUS, RH or MULTI atom file.

    Parameters
    ----------
    filename: str
        String with atom file name.
    format: str
        Can be 'RH' (default), 'EBYSUS', 'DIPER' or 'MULTI'.
    """

    def __init__(self, filename, format="RH", fdir="./"):
        self.read_atom(os.path.join(fdir, filename), format)

    def __repr__(self):
        """
        called when doing str(self); gives informative details about self.
        """
        return "<AtomFile with atom='{}', nlevel={}. (object={})>".format(
            self.element, self.nlevel, object.__repr__(self)
        )

    @staticmethod
    def read_atom_levels(data, format="RH"):
        """
        Reads levels part of atom file.
        """
        tmp = []
        dtype = [
            ("energy", "f8"),
            ("g_factor", "f8"),
            ("label", "|U30"),
            ("stage", "i4"),
            ("level_no", "i4"),
        ]
        if (format.upper() == "EBYSUS") or (format.upper() == "RH"):
            extra_cols = 2
        elif (format.upper() == "MULTI") or (format.upper() == "DIPER"):
            extra_cols = 1
            dtype = dtype[:-1]
        else:
            raise ValueError("Format must be EBYSUS, DIPER, RH or MULTI")
        for line in data:
            buf = line.split("'")
            assert len(buf) == 3
            tmp.append(tuple(buf[0].split() + [buf[1].strip()] + buf[2].split()[:extra_cols]))
        return np.array(tmp, dtype=dtype)

    def read_atom(self, filename, format="RH"):
        self.format = format.upper()
        data = []
        counter = 0
        with open(filename, "r") as atom_file:
            for line in atom_file:
                tmp = line.strip()
                # clean up comments and blank lines
                if not tmp:
                    continue
                if tmp[0] in ["#", "*"]:
                    continue
                data.append(tmp)
        self.element = data[counter].lower()
        counter += 1
        if self.format == "EBYSUS":
            self.units = {
                "level_energies": units.Unit("1/cm"),
                #'line_wavelength': units.Unit('Angstrom'),
                "line_stark": units.Unit("cm"),
                "continua_photoionisation": units.Unit("cm2"),
                "continua_wavelength": units.Unit("Angstrom"),
                "collision_cross_sections": units.Unit("cm3"),
            }
            self.abund = float(data[counter].split()[0])
            self.atomic_weight = float(data[counter].split()[1])
            ### We may consider to add number of protons.
            counter += 1
        elif self.format == "DIPER":
            self.units = {
                "level_energies": units.Unit("1/cm"),
                "line_wavelength": units.Unit("Angstrom"),
                "line_stark": units.Unit("cm"),
                "continua_photoionisation": units.Unit("cm2"),
                "continua_wavelength": units.Unit("Angstrom"),
                "collision_cross_sections": units.Unit("cm3"),
            }
            self.abund = float(data[counter].split()[0])
            self.atomic_weight = float(data[counter].split()[1])
            ### We may consider to add number of protons.
            counter += 1
        elif self.format == "RH":
            self.units = {
                "level_energies": units.Unit("J m / cm"),
                "line_wavelength": units.Unit("nm"),
                "line_stark": units.Unit("m"),
                "continua_photoionisation": units.Unit("m2"),
                "continua_wavelength": units.Unit("nm"),
                "collision_cross_sections": units.Unit("m3"),
            }
        elif self.format == "MULTI":
            self.units = {
                "level_energies": units.Unit("J m / cm"),
                "line_wavelength": units.Unit("Angstrom"),
                "line_stark": units.Unit("cm"),
                "continua_photoionisation": units.Unit("cm2"),
                "continua_wavelength": units.Unit("Angstrom"),
                "collision_cross_sections": units.Unit("cm3"),
            }
            self.abund = float(data[counter].split()[0])
            self.atomic_weight = float(data[counter].split()[1])
            counter += 1
        else:
            raise ValueError("Unsupported atom format " + format)
        nlevel, nline, ncont, nfixed = np.array(data[counter].split(), dtype="i")
        self.nlevel = nlevel
        self.nline = nline
        self.ncont = ncont
        self.nfixed = nfixed
        counter += 1
        # read levels
        self.levels = self.read_atom_levels(data[counter : counter + nlevel], self.format)
        counter += nlevel

        # read lines
        tmp = StringIO("\n".join(data[counter : counter + nline]))
        if self.format == "EBYSUS":
            data_type = [
                ("level_start", "i4"),
                ("level_end", "i4"),
                ("f_value", "f8"),
                ("type", "U10"),
                ("nlambda", "i"),
                ("symmetric", "U10"),
                ("qcore", "f8"),
                ("qwing", "f8"),
                ("vdApprox", "U10"),
                ("vdWaals", "f8", (4,)),
                ("radiative_broadening", "f8"),
                ("stark_broadening", "f8"),
            ]
        if self.format == "DIPER":
            data_type = [
                ("level_start", "i4"),
                ("level_end", "i4"),
                ("value1", "f8"),
                ("iter1", "i"),
                ("value2", "f8"),
                ("value3", "f8"),
                ("value4", "f8"),
                ("iter2", "i"),
                ("value5", "f8", (3,)),
            ]
        elif self.format == "RH":
            data_type = [
                ("level_start", "i4"),
                ("level_end", "i4"),
                ("f_value", "f8"),
                ("type", "U10"),
                ("nlambda", "i"),
                ("symmetric", "U10"),
                ("qcore", "f8"),
                ("qwing", "f8"),
                ("vdApprox", "U10"),
                ("vdWaals", "f8", (4,)),
                ("radiative_broadening", "f8"),
                ("stark_broadening", "f8"),
            ]
        elif self.format == "MULTI":
            data_type = [
                ("level_start", "i4"),
                ("level_end", "i4"),
                ("f_value", "f8"),
                ("nlambda", "i"),
                ("qwing", "f8"),
                ("qcore", "f8"),
                ("iw", "i4"),
                ("radiative_broadening", "f8"),
                ("vdWaals", "f8", (1,)),
                ("stark_broadening", "f8"),
                ("type", "U10"),
            ]
        if nline > 0:
            self.lines = np.genfromtxt(tmp, dtype=data_type)
        else:
            self.lines = []
        counter += nline

        # read photoionization continua
        self.continua = []
        for _ in range(ncont):
            line = data[counter].split()
            if len(line) > 2:
                counter += 1
                result = {}
                result["level_start"] = int(line[0])
                result["level_end"] = int(line[1])
                result["edge_cross_section"] = float(line[2])
                result["nlambda"] = int(line[3])
                if (self.format == "RH") or (self.format == "EBYSUS"):
                    result["wavelength_dependence"] = line[4].upper()
                    result["wave_min"] = float(line[5])
                elif self.format == "MULTI":
                    if float(line[4]) > 0:
                        result["wavelength_dependence"] = "HYDROGENIC"
                    else:
                        result["wavelength_dependence"] = "EXPLICIT"
                if result["wavelength_dependence"] == "EXPLICIT":
                    tmp = "\n".join(data[counter : counter + result["nlambda"]])
                    counter += result["nlambda"]
                    result["cross_section"] = np.genfromtxt(StringIO(tmp))
            elif len(line) == 1:  # This is because He_3.atom has a very different format.
                result = {"nbin": line[0]}
                counter += 1
                result["bin_euv"] = float(data[counter].split())
            elif len(line) == 2:  # This is because He_3.atom has a very different format.
                if "result" in locals():
                    if "bin_euv" not in result:
                        result = {}
                        # JMS default from HION, however, this should be check from HION.
                        result["nbin"] = 6
                        result["bin_euv"] = [
                            911.7,
                            753.143,
                            504.0,
                            227.800,
                            193.919,
                            147.540,
                            20.0,
                        ]
                else:
                    result = {}
                    # JMS default from HION, however, this should be check from HION.
                    result["nbin"] = 6
                    result["bin_euv"] = [
                        911.7,
                        753.143,
                        504.0,
                        227.800,
                        193.919,
                        147.540,
                        20.0,
                    ]
                result["level_start"] = int(line[0])
                result["level_end"] = int(line[1])
                counter += 1
                tmp = "\n".join(data[counter : counter + result["nbin"]])
                dtype = [("index", "i4"), ("cross_section", "f8")]
                value = np.genfromtxt(StringIO(tmp), dtype=dtype)
                counter += result["nbin"]
                result["cross_section"] = value["cross_section"]

            self.continua.append(result)

        # read fixed transitions
        self.fixed_transitions = []
        tmp = StringIO("\n".join(data[counter : counter + nfixed]))
        data_type = [
            ("level_start", "i4"),
            ("level_end", "i4"),
            ("strength", "f8"),
            ("trad", "f8"),
            ("trad_option", "U10"),
        ]
        if nfixed > 0:
            self.fixed_transitions.append(np.genfromtxt(tmp, dtype=data_type))

        counter += nfixed

        # read collisions
        ### IN MULTI FORMAT COLLISIONS START WITH GENCOL
        ### Also in MULTI, must merge together lines that are written in
        ### free format (ie, not prefixed by OMEGA, CE, etc...)
        self.collision_temperatures = []
        self.collision_tables = []
        # Keys for rates given as function of temperature
        COLLISION_KEYS_TEMP = [
            "OHMEGA",
            "OMEGA",
            "CE",
            "CI",
            "CP",
            "CH",
            "CH0",
            "CH+",
            "CR",
            "RECO",
            "OHM",
            "TEMP",
        ]
        # Keys for rates written as single line
        COLLISION_KEYS_LINE = [
            "AR85-CEA",
            "AR85-CHP",
            "AR85-CHH",
            "SHULL82",
            "BURGESS",
            "SUMMERS",
            "AR85-CH",
            "AR85-CHE",
            "PHHY",
        ]
        COLLISION_KEYS_LINES = ["AR85-CDI", "BADNELL", "SPLUPS", "SPLUPS9"]
        COLLISION_KEYS_OTHERS = ["VORONOV", "CEXC"]  # Missing AR85-RR, RADRAT, SPLUPS5.
        ALL_KEYS = COLLISION_KEYS_TEMP + COLLISION_KEYS_LINE + COLLISION_KEYS_LINES + COLLISION_KEYS_OTHERS
        SINGLE_KEYS = ["GENCOL", "END"]

        if (
            (self.format == "MULTI") or (self.format == "EBYSUS") or (self.format == "DIPER")
        ):  # merge lines in free FORMAT
            collision_data = []
            while counter < len(data):
                line = data[counter]
                key = data[counter].split()[0].upper().strip()
                if key in ALL_KEYS:
                    tmp = line
                    while True:
                        counter += 1
                        key = data[counter].split()[0].upper().strip()
                        if key in ALL_KEYS + SINGLE_KEYS:
                            collision_data.append(tmp)
                            break
                        else:
                            tmp += "  " + data[counter]
                elif key in SINGLE_KEYS:
                    collision_data.append(line)
                    counter += 1
        else:
            collision_data = data[counter:]
        unread_lines = False
        counter = 0
        while counter < len(collision_data) - 1:
            line = collision_data[counter].split()
            key = line[0].upper()
            result = {}
            if key == "END":
                break
            elif key == "TEMP":
                temp_tmp = np.array(line[2:]).astype("f")
                self.collision_temperatures.append(temp_tmp)
            # Collision rates given as function of temperature
            elif key in COLLISION_KEYS_TEMP:
                if not hasattr(self, "collision_temperatures"):
                    print("No temperature block" " found before %s table" % (key))
                ntemp = len(temp_tmp)
                result = {
                    "type": key,
                    "level_start": int(line[1]),
                    "level_end": int(line[2]),
                    "temp_index": len(self.collision_temperatures) - 1,
                    "temp": temp_tmp,
                    "data": np.array(line[3 : 3 + ntemp]).astype("d"),
                }  # this will not work in MULTI
                assert len(result["data"]) == len(temp_tmp), (
                    "Inconsistent " "number of points between temperature and collision table"
                )
            elif key in COLLISION_KEYS_LINE:
                if key == "SUMMERS":
                    result = {"type": key, "data": float(line[1])}
                else:
                    result = {
                        "type": key,
                        "level_start": int(line[1]),
                        "level_end": int(line[2]),
                        "data": np.array(line[3:]).astype("f"),
                    }
            elif key in COLLISION_KEYS_LINES:
                assert len(line) >= 4, "%s must have >3 elements" % key
                result = {
                    "type": key,
                    "level_start": int(line[1]),
                    "level_end": int(line[2]),
                }
                if key == "BADNELL":
                    rows = 2
                else:
                    rows = int(line[3])
                result["nshells"] = rows
                if (
                    (self.format == "MULTI") or (self.format == "EBYSUS") or (self.format == "DIPER")
                ):  # All values in one line
                    tmp = np.array(line[4:]).astype("d")
                    assert tmp.shape[0] % rows == 0, "Inconsistent number of" " data points for %s" % key
                    result["data"] = tmp.reshape((rows, tmp.shape[0] // rows))
                else:  # For RH, values written in matrix form
                    tmp = data[counter + 1 : counter + 1 + rows]
                    result["data"] = np.array([l.split() for l in tmp]).astype("d")
                    counter += rows

            elif key in ["CEXC"]:  # JMS we should add wavelength bins here.
                result = {"type": key, "elements": int(line[1])}
                rows = int(line[1])
                tmp = np.array(line[2:]).astype("d")
                assert tmp.shape[0] % int(line[1]) == 0, "Inconsistent number of" " data points for %s" % key
                result["data"] = tmp.reshape((rows, tmp.shape[0] // rows))

            elif key == "VORONOV":
                rows = int(line[1])
                for irow in range(rows):
                    if irow > 0:
                        self.collision_tables.append(result)
                    result = {
                        "type": key,
                        "trans": rows,
                        "level_start": int(line[2 + irow * 7]),
                        "level_end": int(line[3 + irow * 7]),
                    }
                    tmp = StringIO(" ".join(line[4 + irow * 7 :]))
                    data_type = [
                        ("dE", "f8"),
                        ("P", "i"),
                        ("A", "f8"),
                        ("X", "f8"),
                        ("K", "f8"),
                    ]
                    result["data"] = np.genfromtxt(tmp, dtype=data_type)

            elif key == "GENCOL":
                pass
            else:
                unread_lines = True

            if result:
                self.collision_tables.append(result)
            counter += 1

        if unread_lines:
            warnings.warn("Some lines in collision section were not understood", RuntimeWarning)


class MoleculeTools(object):
    """
    Reads data from atom files in Ebysus format.
    """

    def __init__(self, mole_file=""):
        """
        Init file.

        Parameters (INPUT)
        ----------
        atom_file - atom file of interest (in this case, atom_name
                    and stage will be ignore).
        atom_name - lower case letter if the atom of interest.
                    This is mostly just for using some CHIANTI read out.
                    If atom is not defined then it returns a list of all
                    abundances in the file.
            In this case, one will access to it by, e.g., var['h'],
        stage - This is for the CHIANTI read out options.
        """
        if mole_file != "":
            self.mole_file = mole_file
            self.read_molecule_file()

    def get_moldisstg(self, tg, moleculefit):
        if moleculefit == "KURUCZ_85":
            t = tg * 1.0e-4
            pf = self.molparams["pf_coef"][0]
            for icoef in range(1, self.molparams["pf_ncoef"]):
                pf = pf * t + self.molparams["pf_coef"][icoef]

            return np.exp(pf)

        if moleculefit == "KURUCZ_70":
            pf = self.molparams["pf_coef"][0]
            for icoef in range(1, self.molparams["pf_ncoef"]):
                pf = pf * tg + self.molparams["pf_coef"][icoef]

            return np.exp(pf)

        # if moleculefit == 'SAUVAL_TATUM_84':
        #    t = log10(THETA0 / tg)
        #    pf = self.molparams['pf_coef'][0]
        #    for icoef in range(1,self.molparams['pf_ncoef']):
        #        pf = pf*t + self.molparams['pf_coef'][icoef]

        #    pf = POW10(pf)

        if moleculefit == "IRWIN_81":
            t = np.log(tg)
            pf = self.molparams["pf_coef"][0]
            for icoef in range(1, self.molparams["pf_ncoef"]):
                pf = pf * t + self.molparams["pf_coef"][icoef]

            return np.exp(pf)

    def read_molecule_file(self):
        """
        Reads the atom (command style) ascii file into dictionary.
        """

        def readnextline(lines, lp):
            line = lines[lp]
            while line == "\n":
                lp += 1
                line = lines[lp]
            while len(line) < 1 or line[0] == "#" or line[0] == "*":
                lp += 1
                line = lines[lp]
            line = line.split(";")[0].split(" ")
            while "\n" in line:
                line.remove("\n")
            while "" in line:
                line.remove("")
            return line, lp + 1

        li = 0
        params = {}
        # go through the file, add stuff to dictionary
        ii = 1
        # nl = sum(1 for line in open(atomfile))
        f = open(self.mole_file)
        headers = ["KURUCZ_85"]  # Missing AR85-RR, RADRAT, SPLUPS5. AR85-CHE not used
        lines = f.readlines()
        f.close()
        lp = li
        while True:
            line, lp = readnextline(lines, lp)
            if len(line) == 0:
                break
            if (line[0].strip().lower() == "##") and (line[1].strip().lower() == "end"):
                break

            if (np.size(line) == 1) and (ii == 1):
                params = {"molecule": line[0].strip()}
                li += 1
            elif ii == 2:
                if np.size(line) == 1:
                    params["charge"] = int(line[0].strip())
                    li += 1
            elif ii == 3:
                if np.size(line) == 1:
                    params["const_atoms"] = line[0].strip()
                    li += 1
            elif ii == 4:
                if np.size(line) == 1:
                    params["ediss"] = float(line[0].strip())
                    li += 1
            elif ii == 5:
                if np.size(line) == 1:
                    params["mole_fit"] = line[0].strip()
                    li += 1
            elif ii == 6:
                if np.size(line) == 2:
                    params["tmin"] = float(line[0].strip())
                    params["tmax"] = float(line[1].strip())
                    li += 1
            elif ii == 7:
                if np.size(line) > 1:
                    params["pf_ncoef"] = int(line[0].strip())
                    params["pf_coef"] = np.linspace(0, 1, params["pf_ncoef"])
                    for icoef in range(1, params["pf_ncoef"]):
                        params["pf_coef"][icoef - 1] = float(line[icoef].strip())
                    li += 1
            elif ii == 8:
                if np.size(line) > 1:
                    params["eqc_ncoef"] = int(line[0].strip())
                    params["eqc_coef"] = np.linspace(0, 1, params["eqc_ncoef"])
                    for icoef in range(1, params["eqc_ncoef"]):
                        params["eqc_coef"][icoef - 1] = float(line[icoef].strip())
                    li += 1
            ii += 1

        self.molparams = params


"""
TOOLS
"""


def pop_over_species_atomf(
    ntot,
    Te,
    atomfiles=None,
    threebody=True,
    gencol_key="voronov",
    tau=None,
    verbose=True,
):
    """
    compute populations assuming statistical equilibrium.
    Parameters
    ----------
    ntot: array
        total number density [cm^-3]
    Te: array
        temperature (assumes all species have the same temperature)
    atomfiles: list of strings
        files with atom data
    threebody: True (default) or False
        whether to include three-body ionization/recombination effects.
    gencol_key: 'voronov' (default) or 'atomic'
        indicates which data (ion/rec rates) to read from the atom files.
    tau: None (default), number or array
        optical depth (?)
        None --> ignore
    verbose: True (default) or False
        verbosity for Bifrost_units creation

    Returns
    -------
    dict with keys=names of elements; values=lists of number densities of fluids
        also, 'nel' gives number density of electrons.
        Example:
        {'h': [nHI, nHII], 'he': [nHeI, nHeII, nHeIII], 'nel': n_electrons}
    """
    from helita.sim.bifrost import Bifrost_units

    if atomfiles is None:
        atomfiles = ["H_2.atom", "He_3.atom"]
    units = Bifrost_units(verbose=verbose)
    totabund = 0.0
    for isp in range(0, len(atomfiles)):
        atominfo = Atom_tools(atom_file=atomfiles[isp])
        totabund += atominfo.get_abund(Chianti=True)

    all_pop_species = {}
    nel = 0
    for isp in range(0, len(atomfiles)):
        print("Starting with atom", atomfiles[isp], whsp * 5, end="\r", flush=True)
        atominfo = Atom_tools(atom_file=atomfiles[isp])
        abund = np.array(atominfo.get_abund(Chianti=True))
        atominfo.get_atomweight() * units.amu
        n_species = np.zeros((np.shape(ntot)))
        n_species = ntot * (np.array(abund / totabund))
        atominfo.inv_pop_atomf(
            n_species,
            Te,
            niter=100,
            threebody=threebody,
            gencol_key=gencol_key,
            tau=tau,
        )
        all_pop_species[atominfo.atom] = atominfo.n_isp
        all_pop_species[atominfo.atom + "info"] = atominfo
        nel += atominfo.n_el
        print("Done with atom", atomfiles[isp], whsp * 5, end="\r", flush=True)
    all_pop_species["nel"] = nel
    return all_pop_species


def pop_over_species_fluids(ntot, Te, atomfiles, fluids, attr="nr", **kw):
    """
    calculates and assigns number densities to attr (default 'nr') of fluids,
    based on total number density and temperature. All number densities will be
    the same units as ntot (which must(?) be [cm^-3])

    This is just a convenient interface to pop_over_species_atomf.

    Returns electron number density.
    """
    x = pop_over_species_atomf(ntot, Te, atomfiles, **kw)
    for element in fluids.element:
        elt_fluids = where_attr_is("element", element)
        setattr(elt_fluids, attr, x[element])
    return x["nel"]


def comp_tau(r, dz, cmass_tau=None):
    """
    Compute approximation to tau in manner of Carlsson & Leenaarts 2012.

    "Approximations for radiative cooling and heating in the solar
    chromosphere" Carllson, M & Leenaarts, J. Astronomy & Astrophysics,
    Volume 539, id.A39, 10 pp.
    """
    from helita.sim.bifrost import Bifrost_units

    units = Bifrost_units()
    if cmass_tau == None:
        cmass_tau = (-0.1, 0.2)
    cmass = np.zeros(np.shape(r))
    rho = r * units.u_r
    cmass[:, :, 0] = rho[:, :, 0] * dz[0] * units.u_l
    for i in range(1, np.shape(dz)[0], 1):
        cmass[:, :, i] = cmass[:, :, i - 1] + 0.5 * (rho[:, :, i] + rho[:, :, i - 1]) * dz[i] * units.u_l
    tau = cmass * 10.0 ** (-1 * cmass_tau[0])
    return -1.0 * np.log(np.clip(np.squeeze(np.exp(-cmass_tau[1] * tau) / tau**0.3), 1.0e-30, 1.0))


def diper2eb_atom_ascii(atomfile, output):
    """
    Writes the atom (command style) ascii file into dictionary.
    """
    num_map = [
        (1000, "M"),
        (900, "CM"),
        (500, "D"),
        (400, "CD"),
        (100, "C"),
        (90, "XC"),
        (50, "L"),
        (40, "XL"),
        (10, "X"),
        (9, "IX"),
        (5, "V"),
        (4, "IV"),
        (1, "I"),
    ]

    def num2roman(num):
        """
        converts integer to roman number.
        """
        roman = ""
        while num > 0:
            for i, r in num_map:
                while num >= i:
                    roman += r
                    num -= i
        return roman

    def copyfile(scr, dest):
        try:
            shutil.copy(scr, dest)
        except shutil.Error as e:  # scr and dest same
            print("Error: %s" % e)
        except IOError as e:  # scr or dest does not exist
            print("Error: %s" % e.strerror)

    datelist = []
    today = datetime.date.today()
    datelist.append(today)
    """ Writes the atom (command style) ascii file into dictionary """
    text0 = [
        "# Created on "
        + str(datelist[0])
        + " \n"
        + "# with diper2eb_atom_ascii only for ground ionized levels \n"
        + "# the atom file has been created using diper 1.1, "
        + "# REGIME=1, APPROX=1 \n"
    ]

    # No clue where to get those.
    phcross = [
        0.00000000000,
        0.00000000000,
        4.9089501e-18,
        1.6242972e-18,
        1.1120017e-18,
        9.3738273e-19,
    ]
    len(phcross)
    copyfile(atomfile, output)
    f = open(output, "r")
    data = f.readlines()
    f.close()
    for v in range(0, len(data)):
        data[v] = data[v].replace("*", "#")
    data = data[0:2] + [str(data[2]).upper()] + [data[3]] + [str(data[4]).upper()] + data[5:]
    data = text0 + data
    text = [
        "# nk is number of levels, continuum included \n"
        + "# nlin is number of spectral lines in detail \n"
        + "# ncont is number of continua in detail \n"
        + "# nfix is number of fixed transitions \n"
        + "#   ABUND   AWGT \n"
    ]
    data = data[0:3] + text + data[4:]
    line = data[2]
    line = line.split(";")[0].split(" ")
    while "" in line:
        line.remove("")
    atom = str(line[0])
    atom = atom.replace("\n", "")
    data[2] = " " + atom + "\n"
    data = data[:5] + ["#    NK NLIN NCNT NFIX \n"] + data[6:]
    line = data[6]
    line = line.split(";")[0].split(" ")
    while "" in line:
        line.remove("")

    nk = int(line[0])
    nlin = int(line[1])
    ncont = int(line[2])
    nfix = int(line[3])

    data[6] = (
        "    {0:3d}".format(nk)
        + "  {0:3d}".format(nlin)
        + "  {0:3d}".format(ncont)
        + "  {0:3d}".format(nfix)
        + "\n"
    )

    text = [
        "#        E[cm-1]     g              label[35]                   "
        + "stg  lvlN \n"
        + "#                        '----|----|----|----|----|----|----|'\n"
    ]
    data = data[0:7] + text + data[7:]

    for iv in range(8, 8 + nk):
        line = data[iv]
        line = line.split(";")[0].split(" ")
        while "" in line:
            line.remove("")
        while "'" in line:
            line.remove("'")
        line[2] = line[2].replace("'", "")
        strlvl = [" ".join(line[v].strip() for v in range(2, np.size(line) - 1))]
        # the two iv are wrong at the end...
        data[iv] = (
            "    {0:13.3f}".format(float(line[0]))
            + "  {0:5.2f}".format(float(line[1]))
            + " ' {0:2}".format(atom.upper())
            + " {0:5}".format(num2roman(int(line[-1])))
            + " {0:26}".format(strlvl[0])
            + "'  {0:3d}".format(int(line[-1]))
            + "   {0:3d}".format(iv - 7)
            + "\n"
        )

    headers = [
        "GENCOL",
        "CEXC",
        "AR85-CDI",
        "AR85-CEA",
        "AR85-CH",
        "AR85-CHE",
        "CI",
        "CE",
        "CP",
        "OHM",
        "BURGESS",
        "SPLUPS",
        "SHULL82",
        "PHHY",
        "TEMP",
        "RECO",
        "VORONOV",
        "EMASK",
    ]  # Missing AR85-RR, RADRAT, SPLUPS5. AR85-CHE is not used

    textar85cdi = [
        "# Data for electron impact ionization Arnaud and Rothenflug  \n"
        + "# (1985) updated for Fe ions by Arnaud and Rothenflug (1992) \n"
        + "# 1/(u I^2) (A (1 - 1/u) + B (1 - 1/u)^2) + C ln(u) + "
        + " D ln(u)/u) (cm^2)  \n"
        + "#   i   j \n"
    ]

    textar85cdishell = ["# Numbers of shells \n"]
    textar85cdiparam = ["# dE(eV)  A   B   C   D \n"]

    textar85ct = [
        "# Data for charge transfer rate of ionization and recombination"
        + "# Arnaud and Rothenflug (1985) \n"
        + "# updated for Fe ions by Arnaud and Rothenflug (1992) \n"
    ]
    textar85cea = [
        "# Data authoionization following excitation Arnaud and "
        + "Rothenflug (1985) \n"
        + "# (this is a bit of caos... uses different expression for different"
        + "# species) See appendix A.  \n"
        + "#   i   j \n"
    ]

    textshull82 = [
        "# Recombination rate coefficients Shull and Steenberg (1982) \n"
        + "# provides direct collisional ionization with the following \n"
        + "# fitting: \n"
        + "# Ci  = Acol T^(0.5) (1 + Ai T / Tcol)^(-1) exp(-Tcol/T), with\n"
        + "# Ai ~ 0.1 for the recombination rate combines the sum of radiative\n"
        "# + and dielectronic recombination rate \n"
        + "# alpha_r = Arad (T_4)^(-Xrad) ; and alpha_d = Adi T^(-3/2)"
        + " exp(-T0/T) (1+Bdi exp(-T1/T))\n"
        + "#   i  j   Acol     Tcol     Arad     Xrad      Adi      Bdi      "
        + " T0       T1 \n"
    ]

    textphhy = [
        " # Photoionization rate for hydrogen like atoms \n"
        + " # Ci = 8.*pi*alpha*Gaunc*nc*nu^3/c^2*expint1(dE/k_B/Trad) \n"
        + " # assumes J_nu = 2*h_Planck*nu^3/c^2*exp(-h_Planck/k_B/Trad) \n"
        + " # and alpha_nu = alpha_0*(nu_ion_edge/nu)^3; see \n"
        + " # Vernazza, Avrett, Loeser (1981) ApJS, vol 45, p. 662 \n"
        + " # Note that the rate is reduced by a factor exp(-h_Planck*nu/k_B/Te) \n"
        + " # where h_Planck*nu is the energy from the ground state to the state \n"
        + " # photoionization is active: for hydrogen like atoms (on the Sun) \n"
        + " # photoionization occurs mainly from the first excited state. So, \n"
        + " # this formula is not correct for He I as it is NOT hydrogen like... \n"
        + " # but it should not be too horrible as a starting point. Trad should \n"
        + " # be replaced with Te at high opacity (in photosphere and below), \n"
        + " # but if this is important then you should be solving the full \n"
        + " # radiative transport equation. \n"
    ]

    textar85ch = [
        "# charge transfer recombination with neutral hydrogen Arnaud\n"
        + "#  and Rothenflug (1985) updated for Fe ions by Arnaud and \n"
        + "# Rothenflug (1992) \n"
        + "# alpha = a (T_4)^b (1 + c exp(d T_4)) \n"
        + "#   i   j \n"
    ]
    textar85chparam = ["#   Temperature range (K)   a(1e-9cm3/s)    b      c    d \n"]
    textar85chem = [
        "# charge transfer recombination with ionized hydrogen Arnaud and \n"
        + "# Rothenflug (1985) \n"
        + "# updated for Fe ions by Arnaud and Rothenflug (1992) \n"
        + "# alpha = a (T_4)^b (1 + c exp(d T_4)) \n"
        + "#   i   j \n"
    ]

    # if 'SHULL82\n' in data:
    try:
        iloc = data.index("SHULL82\n")
        if iloc > 1:
            data = data[0 : iloc + 1] + textshull82 + data[iloc + 1 :]
    except BaseException:
        print("no key")

    # if 'PHHY\n' in data:
    try:
        iloc = data.index("PHHY\n")
        if iloc > 1:
            data = data[0 : iloc + 1] + textphhy + data[iloc + 1 :]
    except BaseException:
        print("no key")

    # if 'AR85-CHE+\n' in data:
    try:
        iloc = data.index("AR85-CHE+\n")
        if iloc > 1:
            data = data[0 : iloc + 1] + textar85chem + data[iloc + 1 :]
            data = data[0 : iloc + 3] + textar85chparam + data[iloc + 3 :]
    except BaseException:
        print("no key")
    # if 'AR85-CH\n' in data:
    try:
        iloc = data.index("AR85-CH\n")
        if iloc > 1:
            data = data[0 : iloc + 1] + textar85ch + data[iloc + 1 :]
            data = data[0 : iloc + 3] + textar85chparam + data[iloc + 3 :]
    except BaseException:
        print("no key")

    # if 'AR85-CDI\n' in data:
    try:
        iloc = data.index("AR85-CDI\n")
        if iloc > 1:
            data = data[0 : iloc + 1] + textar85cdi + data[iloc + 1 :]
            data = data[0 : iloc + 3] + textar85cdishell + data[iloc + 3 :]
            data = data[0 : iloc + 5] + textar85cdiparam + data[iloc + 5 :]
    except BaseException:
        print("no key")

    # if 'AR85-CEA\n' in data:
    try:
        iloc = data.index("AR85-CEA\n")
        if iloc > 1:
            data = data[0 : iloc + 1] + textar85cea + data[iloc + 1 :]
    except BaseException:
        print("no key")

    f = open("temp.atom", "w")
    for i in range(0, len(data)):
        f.write(data[i])

    if "GENCOL\n" not in data:
        f.write("GENCOL\n")
    else:
        print("gencol is in data")
    f.close()
    add_voro_atom("temp.atom", output, atom=atom.lower(), nk=nk)


def add_voro_atom(inputfile, outputfile, atom="", vorofile=DEFAULT_VORONOV_FILE, nk="100"):
    """
    Add voronov information at the end of the atom file.

    Parameters
    ----------
    inputfile - name of the input atom file
    outputfile - name of the output atom file which will include the VORONOV
            information
    atom - lower case letters of the atom of interest. Make sure that it
            matches with the atom file.
    vorofile - voronot table file.
    """

    shutil.copy(inputfile, outputfile)
    atom = atom.lower()
    params = read_voro_file(vorofile)
    f = open(inputfile, "r")
    f.readlines()
    f.close()
    infile = open(inputfile)
    f = open(outputfile, "w")
    for line in infile:
        if not ("END" in line):
            f.write(line)
    infile.close()

    f.write("\n")
    f.write("VORONOV\n")

    f.write(
        "# from Voronov fit formula for ionization rates by \n"
        + "# electron impact by G. S. Voronov: \n"
        + "# ATOMIC DATA AND NUCLEAR DATA TABLES 65, 1-35 (1997) \n"
        + "# ARTICLE NO. DT970732\n"
        + "# <cross> = A (1+P*U^(1/2))/(X + U)*U^K e-U (cm3/s) with U = dE/Te\n"
    )

    strat = ""
    if len("".join(x for x in atom if x.isdigit())) == 0:
        strat = "_1"

    # where I need to add a check if atom is the same as the one in the atom
    # file or even better use directly the atom file info for this.
    atominfo = Atom_tools(atom_file=inputfile)
    atominfo.get_atomZ(atom=atom + strat)
    # tr_line=np.where(atominfo.params['voronov'][0][:,0] == lvl_str+1)
    # phion = self.params['voronov'][0][tr_line[0],2] # get_atomde(atom,
    # Chianti=False)  # 13.6
    # A = self.params['voronov'][0][tr_line[0],4] * 1e6 # get_atomA(atom) *
    # 1.0e6  # converted to SI 2.91e-14
    # X = self.params['voronov'][0][tr_line[0],5] # get_atomX(atom)  # 0.232
    # K = self.params['voronov'][0][tr_line[0],6] # get_atomK(atom)  # 0.39
    # P = self.params['voronov'][0][tr_line[0],3] # get_atomP(atom)  # 0

    f.write(str(atominfo.Z) + "\n")
    jj = 1
    f.write("#   i    j    dE(eV)     P  A(cm3/s)   X      K  \n")
    for ii in range(0, params["NLVLS_MAX"][0]):
        if (atominfo.Z == int(params["SPECIES"][ii, 0])) and jj < nk:
            strat = ""
            if len("".join(x for x in params["SPECIES"][ii, 1] if x.isdigit())) == 0:
                strat = "_1"
            f.write(
                " {0:3d}".format(jj)
                + " {0:3d}".format(jj + 1)
                + " {0:9.3f}".format(get_atomde(atom=params["SPECIES"][ii, 1] + strat, Chianti=False))
                + " {0:3}".format(get_atomP(atom=params["SPECIES"][ii, 1]))
                + " {0:7.3e}".format(get_atomA(atom=params["SPECIES"][ii, 1]))
                + " {0:.3f}".format(get_atomX(atom=params["SPECIES"][ii, 1]))
                + " {0:.3f}".format(get_atomK(atom=params["SPECIES"][ii, 1]))
                + "\n"
            )
            jj += 1
    f.write("END")
    f.close()


def create_goftne_tab(ionstr="fe_14", wvlr=None, abundance="sun_photospheric_1998_grevesse"):
    """
    This allows to calculate GOFT tables in a similar fashion as we do in IDL.
    """
    import ChiantiPy.core as ch
    import periodictable as pt

    if wvlr is None:
        wvlr = [98, 1600]
    ntemp = 101
    neden = 81
    temp = 10.0 ** (4.0 + 0.05 * np.arange(ntemp))
    dens = 10 ** (np.arange(neden) * 0.1 + 6)
    gofnt = np.zeros((ntemp, neden))
    for iden in range(0, neden):
        ion = ch.ion(ionstr, temperature=temp, eDensity=dens[iden], abundance=abundance)
        ion.emiss()  # (erg/s/sr)
# old code
#        ion.populate()
#        ion.intensity()
#        ion.gofnt(wvlRange=wvlr, top=1, plot=False)
        iwvl = np.argmin(np.abs(np.array(ion.Wgfa["wvl"]) - np.mean(wvlr)))
        gofnt[:, iden] = np.squeeze(
            ion.Abundance * ion.IoneqOne / dens[iden] * ion.Emiss["emiss"][iwvl])
# old code
#            [np.where(np.abs(np.array(ion.Wgfa["wvl"]) - wvlr[0]) < wvlr[1] - wvlr[0])[0]])
        if iden == 0:
            print('Computing {} {}'.format(getattr(ion,"IonStr"),ion.Wgfa["wvl"][iwvl]))
    ion.Gofnt = {}
    ion.Gofnt["gofnt"] = gofnt
    ion.Gofnt["eDensity"] = dens
    ion.Gofnt["temperature"] = temp
    ion.Gofnt["wvl"] = ion.Wgfa["wvl"][iwvl]
    try:
        ion.mass = pt.elements[ion.Z].ion[ion.Ion].mass
    except BaseException:
        ion.mass = pt.elements[ion.Z].mass
    if os.environ.get("GOFT_PATH") is None:        
        path = os.path.join(os.environ["BIFROST"],"PYTHON/br_int/br_ioni/data/")
        print('GOFT_PATH not set, saving file in {}'.format(path))
    else:
        path = os.environ.get("GOFT_PATH")
        print('Saving file in {}'.format(path))
    name = getattr(ion, "IonStr") + "_" + str(ion.Gofnt["wvl"]) + ".opy"
    filehandler = open(path + name, "wb")
    pickle.dump(ion, filehandler)

def add_goftab(filenames=None):
    nf = np.size(filenames)
    for ifile in range(0, nf):
        fileh = open(filenames[ifile], "rb")
        table = pickle.load(fileh)
        if ifile == 0:
            goftab = table.Gofnt["gofnt"]
        else:
            goftab += table.Gofnt["gofnt"]
    return goftab


def norm_ne_goftab(table=None):
    nne = np.shape(table)[1]
    tablenorm = table * 1.0
    for i in range(0, nne):
        tablenorm[:, i] = (table[:, i] + 1e-40) / (table[:, 30] + 1e-40)

    return tablenorm


def const_press_tab(filename):
    filehandler = open(filename + ".opy", "rb")
    ion = pickle.load(filehandler)
    filehandler.close()
    temp = ion.Gofnt["gofnt"][:, 35] * 1.0
    for ipres in range(71):
        ion.Gofnt["gofnt"][:, ipres] = temp
    filehandler = open(filename + "cp.opy", "wb")
    pickle.dump(ion, filehandler)
    filehandler.close()


def fone(x, id_v):
    # New routine 24-jan-1994
    # calculates f1(x) needed for collisional rates of arnaud and rothenflug
    # modified by p. judge 15-mar-1994
    # for large values of x, use asymptotic limit
    #: modified december 4, 1995 p. judge for higher precision
    #: at high values of argument
    #: fone  13/1-1996  vh  new parameter id  =0 => fone =1 => dfone/dx

    split = 50.0

    if x < split:
        fone_out = np.exp(x) * float(special.expn(1, x))
        if id_v == 1:
            return fone_out - 1.0 / x
        else:
            return fone_out
    else:
        # december 4, 1995 old code       fone= 1./x
        if id_v == 0:
            return (1.0 - 1.0 / x + 2.0 / x / x) / x
        else:
            return (-1.0 + 2.0 / x - 6.0 / x / x) / x / x


def ftwo(x, id_v):
    # New routine 24-jan-1994  p. judge
    # calculates f2(x) needed for collisional rates of arnaud and rothenflug
    # if argument x is < break, then use the eqns 5 and 6 of
    # hummer (1983, jqsrt, 30 281)
    # break given by hummer as 4.0
    # note that the suggested polynomial expansion of arnaud and rothenflug
    # fails for arguments < break- see hummer's original paper.
    # it is unclear if the tabulated ionization fractions of arnaud and rothenflug
    # contain this error.
    #: ftwo  13/1-1996  vh  new parameter id  =0 => fone =1 => dfone/dx

    break_v = 4.0
    pi = 3.14159
    p = np.array(
        [
            1.0,
            2.1658e02,
            2.0336e04,
            1.0911e06,
            3.7114e07,
            8.3963e08,
            1.2889e10,
            1.3449e11,
            9.4002e11,
            4.2571e12,
            1.1743e13,
            1.7549e13,
            1.0806e13,
            4.9776e11,
            0.0000,
        ]
    )
    q = np.array(
        [
            1.0,
            2.1958e02,
            2.0984e04,
            1.1517e06,
            4.0349e07,
            9.4900e08,
            1.5345e10,
            1.7182e11,
            1.3249e12,
            6.9071e12,
            2.3531e13,
            4.9432e13,
            5.7760e13,
            3.0225e13,
            3.3641e12,
        ]
    )

    if x > break_v:
        px = p[0]
        dpxdx = 0.0
        xfact = 1.0
        for i in range(1, 15):
            xfact = xfact / x
            dxfdx = (1.0 - i) * xfact / x
            px = px + p[i] * xfact
            dpxdx = dpxdx + p[i] * dxfdx

        qx = q[0]
        dqxdx = 0.0
        xfact = 1.0
        for i in range(1, 15):
            xfact = xfact / x
            dxfdx = (1.0 - i) * xfact / x
            qx = qx + q[i] * xfact
            dqxdx = dqxdx + q[i] * dxfdx

        if id_v == 0:
            return px / qx / x / x
        else:
            return 1.0 / qx / x / x * (dpxdx - px / qx * dqxdx - 2.0 * px / x)

    else:
        # hummer's equns 5 and 6
        # gamma is euler's constant (abramovicz+stegun)
        gamma = 0.5772156649
        f0x = pi * pi / 12.0
        term = 1.0
        count = 0.0
        fact = 1.0
        xfact = 1.0
        df0xdx = 0.0

        while abs(term / f0x) > 1.0e-8:
            count = count + 1.0
            fact = fact * count
            xfact = xfact * (-x)
            dxfdx = count * xfact / x
            term = xfact / count / count / fact
            df0xdx = df0xdx + dxfdx / count / count / fact
            f0x = f0x + term
            if count > 100.0:
                break
        fact = (np.log(x) + gamma) * (np.log(x) + gamma) * 0.5 + f0x
        face = np.exp(x)
        if id_v == 0:
            return face * fact
        else:
            return face * fact + face * ((np.log(x) + gamma) / x + df0xdx)


def atomnm(i):
    """
    atomnm 94-02-22  new routine: (philip judge) gives atomic number of
    arelement if i is an integer containing e.g. if input  is 1 it will return
    'h' etc.
    """
    ndata = 28
    arelem = np.array(
        [
            "h",
            "he",
            "li",
            "be",
            "b",
            "c",
            "n",
            "o",
            "f",
            "ne",
            "na",
            "mg",
            "al",
            "si",
            "p",
            "s",
            "cl",
            "ar",
            "k",
            "ca",
            "sc",
            "ti",
            "v",
            "cr",
            "mn",
            "fe",
            "co",
            "ni",
        ]
    )

    if (i <= ndata) and (i >= 1):
        return arelem[i]
    else:
        return "  "
