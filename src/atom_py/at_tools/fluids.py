"""
Created on Fri Feb  5 10:19:59 2021.

@author: Sevans

File Purpose: Fluids class. Allows to easily access atoms, but as fluids.

Atoms are stored in atomfiles with many levels.
But for writing snapshots and other reasons, it is useful to know all the fluids.

"fluids" meaning atom-level pairs.
E.g. if list of atoms(levels) is:
    A(1,2), B(1), C(1,2,3,4)
Then the "fluids" are:
    A1, A2, B1, C1, C2, C3, C4

The Fluids class gives an easy way to iterate through fluids,
and track quantities associated with each fluid.

See also the wiki: https://gitlab.com/LMSAL_HUB/sims_hub/eb_hub/atom_py/-/wikis/atom_tools/Fluids

USAGE NOTES / EXAMPLES:
    GETTING STARTED:
        f = Fluids(['H_2.atom', 'He_3.atom']):
        f.atom          #gives list of atom objects
        f.spec          #gives atom numbers for fluids
        f.lvl           #gives level numbers for fluids
        f.SL            #gives list of (species_number,level_number) tuples for fluids.
        f.atomic_weight #gives atomic weights for fluids
        f.stage         #gives ionization stages for fluids (1=neutral, 2=once-ionized, 3=twice-ionized, etc)
        f.name          #gives abbreviated name (atomic symbol and ionization level as roman numeral) of fluid

    INITIALIZE WITH EBYSUSDATA:
        # For convenience, you can initialize with an EbysusData object instead:
        dd = helita.sim.ebysus.EbysusData(...)
        f  = Fluids(dd=dd)

    INITIALIZE WITH MF_PARAMS_FILE:
        # For convenience, you can initialize with an mf_param_file instead:
        filename = 'name/for/mf_param_file.in'
        f  = Fluids(mf_param_file=filename)

    ADVANCED USAGE:
        f = Fluids(['H_2.atom', 'He_3.atom'])
        # set 'temperature' to 4000 for each fluid in fluids:
        f.temperature = 4000

        # set 'n' to 1e20 for HI, 1e15 for HII, 1e14 for HeI, 1e10 for HeII, 1e9 for HeIII:
        f.n = [1e20, 1e15, 1e14, 1e10, 1e9]
        # Note: if the value has a length and the length == number of fluids, sets fluids[i] = value[i].
        # CAUTION:
        #   Care is needed for something like f.v = [vx, vy, vz],
        #   because if there are 3 fluids you will end up with f[0].v==vx, f[1].v==vy, f[2].v==vz.
        #   This ambiguity can be avoided via "setcommon":
        #     f.setcommon('v') = [vx, vy, vz]  #will ensure each fluid gets [vx, vy, vz].

        # access a parameter you have set:
        f.temperature
        # >> [4000, 4000, 4000, 4000, 4000]
        f[2].temperature
        # >> [4000]
        f[2].n
        # >> [1e14]

        # get a Fluids-like object containing only the neutrals #or only the ions
        f.neutrals()             #use f.ions() for ions

        # set a parameter value 'ux' to 7 for neutrals, 0 for ions:
        f.neutrals().ux = 7
        f.ions().ux     = 0
        # check value of ux:
        f.ux
        # >> [7, 0, 7, 0, 0]

        # get species,level for i'th fluid:
        species, level = f[i][0], f[i][1]      # equivalent: species, level = f[i]

        # get fluid with a given species, level:
        f[(species, level)]    # <-- generic
        f[(1,2)]               # <-- example

        # get fluid with a given name (case-insensitive):
        f[name]                # <-- generic
        f['He II']             # <-- example

    SUPERUSER USAGE:
        # set an attribute of the Fluids class object (makes f.__dict__[attr]=value); attr should be a string.
        f.setattr(attr, value)

        # flexibly set a scalar or vector (see function documentation for more help):
        f.assign_scalars(attr, scalars)
        f.assign_vectors(attr, vectors)

        # turn off automatically converting results to numpy:
        f.numpy = False           # set to True to restore default.

CHECKING EXISTENCE, GETTING ATOMFILES FROM INPUT/ATOMS_MS:
    # get existing atom files in local directory:
    atomfiles()['local']
    # get existing atom files in remote location (INPUT/ATOMS_MS):
    atomfiles()['remote']
    # get remote location (as abspath):
    atomfiles()['remote_location']

    # get existing atom files in a single list, LOCAL FIRST, then remote, in alphabetical order by atom file name:
    get_atomfiles(priority='local')  #this is the default priority.
    # get existing atom files in a single list, REMOTE FIRST, then local, in alphabetical order by atom file name:
    get_atomfiles(priority='remote')

    # get whichever of ['H_2', 'He_3', 'p'] exist in local directory:
    atomfiles(['H_2', 'He_3', 'p'])['local']
    # get whichever of ['H_2', 'He_3', 'p'] exist in remote location (INPUT/ATOMS_MS):
    atomfiles(['H_2', 'He_3', 'p'])['remote']

    # get existing atom files ['H_2', 'He_3', 'p'] ('.atom' extension optional) in a single list,
    # priotitizing local files if files are found in local & remote locations,
    # maintining the order as entered ('H_2' then 'He_3' then 'p'),
    # and returning files as absolute paths:
    get_atomfiles(['H_2', 'He_3', 'p'], priority='local') # priority='local' is the default, you don't need to write it.
    >>
    ['/Users/YourUsername/...pathto.../ebysus/INPUT/ATOMS_MS/H_2.atom',
     '/Users/YourUsername/path/to/local/directory/He_3.atom',
     '/Users/YourUsername/...pathto.../ebysus/INPUT/ATOMS_MS/p.atom']
    # (output ^ assumes 'He_3' exists in local; 'H_2' and 'p' are in remote but not local)


TODO (maybe):
    Automatic fluid ordering: order using the same rules ebysus uses to determine fluid order.
    (sort by increasing atomic number, I assume... ? )

    Be better about computation time: once stored as an array for Fluids,
        have Fluid objects index array, instead of splitting array and remaking every time.
"""


# set defaults
ATOMS_MS_ENVIRON = "INPUT/ATOMS_MS"  # where to look for default atom files <<when using func>>
HIDE_DECORATOR_TRACEBACKS = True

import os
import re
import functools

import numpy as np

from . import atom_tools as at

"""------------------------- Convert atoms to at.Atom_tools objects -------------------------"""


def get_atom(x, errorchecker=[0], priority="local", ATOMS_MS=ATOMS_MS_ENVIRON):
    """
    return dict(atom=(atom object), file=(filename, if available)) x==string.

    -> atom=Atom_tools(atom_file=x), file=x x==atom object -> atom=x, file=None
    x==something else -> atom=None, file=None.

    if x is a string, and x is not an existing file, searches ATOMS_MS
    for x before giving up.
    """
    if isinstance(x, str):
        x = get_atomfiles([x], priority=priority, ATOMS_MS=ATOMS_MS)[0]
        return dict(atom=at.Atom_tools(atom_file=x), file=x)
    elif isinstance(x, at.Atom_tools):
        return dict(atom=x, file=None)
    else:
        errorchecker[0] = 1
        type_errmsg = "Expected string or at.Atom_tools but got type(x)={}, x={}"
        if "Atom_tools" in str(type(x)):
            type_errmsg += (
                "\n\nCommon cause: one package using an older version of fluids.py. "
                "This may fix it: reload all packages then rerun code."
            )
        raise TypeError(type_errmsg.format(type(x), x))


def get_atoms(x, priority="local", ATOMS_MS=ATOMS_MS_ENVIRON):
    """return dict(atoms=[list of atom objects], files=[filenames for atoms, if available])
    x==a single atom object -> atoms = [x],                              files = [None]
    x==a single string      -> atoms = [at.Atom_tools(atom_file=x)],     files = [x]
    x==a list of objects    -> atoms = [get_atom(y)['atom'] for y in x], files = [get_atom(y)['file'] for y in x]

    if x is a string, and x is not an existing file, searches ATOMS_MS for x before giving up.
    """
    errorchecker = [0]
    try:
        aa = get_atom(x, errorchecker)
        xislist = False
    except TypeError:
        if errorchecker[0] != 1:  # x is string or at.Atom_tools obj but some other TypeError occurred.
            raise
        else:  # x is not string nor at.Atom_tools obj.
            xislist = True
    if xislist:
        aas = [get_atom(y) for y in x]
        return dict(
            atoms=[aas[i]["atom"] for i in range(len(aas))],
            files=[aas[i]["file"] for i in range(len(aas))],
        )
    else:  # x represents a single atom
        return dict(atoms=[aa["atom"]], files=[aa["file"]])


"""------------------------- get atom & level name, from label -------------------------"""


def label_to_name(label):
    """
    return name for atom (e.g. 'H I', or 'He III') given label (e.g. 'H I 1S
    2SE', or 'HE III0S0 1SE 0').
    """
    splitted = label.split()
    if len(splitted) == 0:
        return "???"
    elif len(splitted) == 1:
        return splitted[0]
    else:
        # pull roman numeral from second term in splitted.
        m = re.match(r"[IVXLC]+", splitted[1])
        if m is None:
            return " ".join([splitted[0].capitalize(), "?"])
        else:
            return " ".join([splitted[0].capitalize(), m.group()])


"""------------------------- small helper functions -------------------------"""


def _repr_list(x):
    """
    returns prettier __repr__ for list x.

    if all objects in x have the same type, only write the type once.
    provide list of ids of objects in x. (if len(x)==0, return [])
    """
    types = [type(a) for a in x]
    if len(x) == 0:
        objs = []
    else:
        if all([t == types[0] for t in types]):
            # str(types[0]) looks like "<class 'classname'>". type0str looks like "classname".
            type0str = str(types[0]).partition("'")[2].rpartition("'")[0]
            ids = [str(hex(id(a))) for a in x]
            plural = "s" if len(x) > 1 else ""
            objs = "<{} object{} at {}>".format(type0str, plural, ", ".join(ids))
        else:
            objs = [object.__repr__(a) for a in x]
    return str(objs)


def get_atoms_from_mf_param_file(filename):
    """
    returns (atomfiles, species numbers) based on mf_param_file filename.

    Example: mf_params_H2He3.in --> (['H_2.atom', 'He_3.atom'], [1, 2]).
    """
    reading_species = False
    spec_nums = []
    atomfiles = []
    with open(filename, "r") as f:
        for i, line in enumerate(f):
            line, _, comment = line.partition("#")  # remove comments (#)
            line, _, comment = line.partition(";")  # remove comments (;)
            line = line.strip()  # remove leading & trailing whitespace
            if len(line) == 0:
                continue  # ignore blank lines
            if reading_species:
                tokenline = line.split()  # split by whitespace
                if len(tokenline) == 1:  # start of next section (not 'SPECIES' anymore)
                    break
                elif len(tokenline) == 3:  # example tokenline: ['01', 'H', 'H_2.atom']
                    spec_nums += [int(tokenline[0])]
                    atomfiles += [tokenline[2]]
                else:
                    errmsg = "Unrecognized line format at line {} of file '{}':\nproccesed_line='{}'"
                    raise ValueError(errmsg.format(i, filename, line))
            else:
                if line == "SPECIES":
                    reading_species = True
                else:
                    continue
    return (atomfiles, spec_nums)


"""------------------------- helper methods for flexible assign data to fluids -------------------------"""


def _is_iterable(x):
    try:
        iter(x)
    except TypeError:
        return False
    else:
        return True


def _assert_long_enough(x, l):
    assert len(x) >= l, "x is too short! Got len(x)=={} but need len(x)>=Nfluid={}".format(len(x), l)


def _shape_scalar(x, l):
    """reshape scalar data to be appropriate for fluids.val = scalar. l should be Nfluids."""
    if _is_iterable(x):
        _assert_long_enough(x, l)
        return x[:l]
    else:
        return x


def _shape_vector(x, l):
    """reshape vector data to be appropriate for fluids.val = vector. l should be Nfluids."""
    if not _is_iterable(x):
        raise TypeError("cannot turn scalar into a list of vectors")
    elif not _is_iterable(x[0]):
        if hasattr(np, "repeat"):  # numpy has been imported properly
            return np.repeat([x], l, axis=0)
        else:  # numpy has not been imported properly; use alternate method.
            return [list(x)] * l
    else:
        _assert_long_enough(x, l=l)
        return x[:l]


def maintain_attrs(*attrs):
    """
    return decorator which restores attrs of obj after running function.

    It is assumed that obj is the first arg of function.
    """

    def attr_restorer(f):
        @functools.wraps(f)
        def f_but_maintain_attrs(obj, *args, **kwargs):
            """
            f but attrs are maintained.
            """
            __tracebackhide__ = HIDE_DECORATOR_TRACEBACKS
            memory = dict()  # dict of attrs to maintain
            for attr in attrs:
                if hasattr(obj, attr):
                    memory[attr] = getattr(obj, attr)
            try:
                return f(obj, *args, **kwargs)
            finally:
                # restore attrs
                for attr, val in memory.items():
                    setattr(obj, attr, val)

        return f_but_maintain_attrs

    return attr_restorer


"""------------------------- Fluid(s) classes -------------------------"""


class Fluid:
    """
    Helper class for Fluids class.

    The intention is for you to just use Fluids class.
    """

    def __init__(self, atom, idx_lvl, spec=None, filename=None):
        """
        idx_lvl=idx of level in atomfile.

        E.g. 0 --> first level shown.
        """
        aa = get_atom(atom)
        self.atom = aa["atom"]
        self.file = aa["file"] if filename is None else filename
        self.idx_lvl = idx_lvl
        self.lvl = self.level_no
        self.spec = spec
        self.SL = (self.spec, self.lvl)
        self.ionization = (
            self.stage - 1
        )  # number of times fluid has been ionized. charge == ionization * q_electron
        self.name = label_to_name(self.label)

    def __repr__(self):
        return "<{} with SL={}; atom={}>".format(object.__repr__(self), self.SL, object.__repr__(self.atom))

    def __str__(self):
        return "<{} with (specie, level)={}, (element, charge)={}>".format(
            object.__repr__(self), self.SL, (self.element, self.ionization)
        )

    def __getitem__(self, i):
        """
        return (self.spec) if 0, (self.lvl) if 1, else raise IndexError.

        This allows to index fluid like it's a (species, level) tuple.
        """
        if i == 0:
            return self.spec
        elif i == 1:
            return self.lvl
        else:
            raise IndexError(
                "index out of range for Fluid object. Valid indices are ",
                "0 (-> species), 1 (-> level), but got index = {:}".format(i),
            )

    # here is where all the magic happens (this is why Fluid can get attributes directly from atom.params)
    def __getattr__(self, attr):
        """
        when self does not have attr, get attr from self.atom.params.

        If that fails, try to get
        self.atom.params.levels[self.idx_level][attr].     If that also
        fails, raise the original AttributeError.
        """
        try:
            return getattr(self.atom.params, attr)
        except AttributeError:
            try:
                return self.atom.params.levels[self.idx_lvl][attr]
            except:
                raise


class Fluids:
    """class which allows easy access to atoms but as a list of fluids (each is one atom and one level) instead.
    you can access any parameters in Fluid or in atom.params (for each atom) as if it were defined in Fluid.
    Example:
        # initialize (two options):
            x = Fluids([at.Atom_tools('H_2.atom'), at.Atom_tools('He_3.atom')])
            # x = Fluids(['H_2.atom', 'He_3.atom']) # same as above, except filenames will also be stored internally.
        # defined in Fluid (in __init__):
            x.atom           # list of atom objects
            x.spec           # atom number for fluids: [1, 1, 2, 2, 2]
            x.lvl            # list of level numbers of fluids: [1, 2, 1, 2, 3]
                             #     Note: this is not necessarily ionization stage, e.g. for multilevel HI atom they will differ.
                             #     use x.stage to get ionization stage.
            x.ionization     # number of times fluid has been ionized. charge == ionization * q_electron
            x.name           # names (atom symbol & ionization level in roman numerals) for fluids:
                             #    ['H I', 'H II', 'He I', 'He II', 'He III']
        # defined in atom.params (see atom.params.__dict__ for available options):
            x.atomic_weight  # list of atomic weights of atoms: [1.0, 1.0, 4.0, 4.0, 4.0]
            x.abund          # list of abundances of atoms: [12.0, 12.0, 11.0, 11.0, 11.0]
        # defined in atom.params.levels:
            x.stage          # list of ionization stages: [1, 2, 1, 2, 3]
            x.level_no       # list of level numbers (equivalent to x.lvl): [1, 2, 1, 2, 3]
            x.label          # list of (labels for) levels of fluids:
                             #    ['H I 1S 2SE', 'H II continuum', 'HE I 1S2 1SE 0', 'HE II 1S 2SE 1/2', 'HE III0S0 1SE 0']
    """

    def __new__(cls, atoms=[], specs=None, numpy=True, dd=None, dd_memory=True, **kwargs):
        """
        create new instance.

        handles dd_memory case. See __init__ for documentation.
        """
        if dd_memory and (dd is not None):
            if hasattr(dd, "_fluids"):
                return (
                    dd._fluids
                )  # __init__ will be called after, because dd._fluids is a subclass of Fluids.
                ## however, __init__ will do nothing because dd=None, dd_memory=False, and hasattr(dd, 'fluids').
        return super(Fluids, cls).__new__(cls)

    def __init__(
        self,
        atoms=[],
        specs=None,
        numpy=True,
        stack=False,
        stack_axis=-1,
        dd=None,
        dd_memory=True,
        mf_param_file=None,
        priority="local",
        ATOMS_MS=ATOMS_MS_ENVIRON,
    ):
        """
        initialize using list of filenames or at.Atom_tools objects.

        specs = species numbers to assign to atoms. if None, default to [1,2,...,len(atoms)]
        numpy = whether to return lists as numpy arrays
        stack = whether to stack lists in a particular way. (only applies if numpy=True)
            This applies to returned numpy arrays, and to input numpy arrays.
            use stack=True in order to do "intuitive" maths with fluids's attrs without knowing shapes.
            For example, fluids.n = fluids.r / fluids.atomic_weight    # works even if r is 3-d.
        stack_axis = -1
            if stack, stack along this axis. (as per np.stack(axis=stack_axis))
        dd    = None (default) or helita.sim.ebysus.EbysusData object
            if entered -> set specs & atoms based on dd.
                          specs = sorted(list(dd.att.keys())); atoms = [dd.att[s] for s in specs]
        dd_memory = bool (default True). Ignored when False or when dd is None.
            True -> dd._fluids = self. and if dd._fluids existed already, set self = dd._fluids.
        mf_param_file = None (default) or string
            if entered -> set specs & atoms based on the data in SPECIES section of file mf_param_file.
        priority = 'local' or 'remote'. Which atomfile to use if atomfile appears locally and in ATOMS_MS.
        ATOMS_MS = path from ebysus to remote location containing atomfiles (default: INPUT/ATOMS_MS).
        """
        remember = dd_memory and (dd is not None)
        if remember:
            if hasattr(dd, "_fluids"):
                return  # do nothing here, because we handled this case already, in __new__.
        if isinstance(specs, str):
            raise TypeError(
                "specs entered as string, but expected None or list of values. "
                "If this is surprising, ensure that you passed atoms as a list! "
                '(You entered specs="{:}")'.format(specs)
            )
        # set self.atoms, self.files, self.specs
        if dd is None:
            if mf_param_file is not None:
                atoms, specs = get_atoms_from_mf_param_file(mf_param_file)
            aas = get_atoms(atoms, priority=priority, ATOMS_MS=ATOMS_MS)
            self.atoms = aas["atoms"]
            self.files = aas["files"]
            self.specs = specs if specs is not None else list(range(1, len(self.atoms) + 1))
        else:
            self.specs = sorted(list(dd.att.keys()))
            self.atoms = [dd.att[spec] for spec in self.specs]
            self.files = [None for spec in self.specs]
        # set self.fluids
        self.fluids = [
            Fluid(atom, i, spec=self.specs[j], filename=self.files[j])
            for j, atom in enumerate(self.atoms)
            for i, level in enumerate(atom.params.levels)
        ]
        self._init_internal(numpy=numpy, stack=stack, stack_axis=stack_axis)
        if remember:  # and, we know dd._fluids doesn't exist, because we checked at start of __init__.
            dd._fluids = self

    def _init_internal(self, numpy=True, stack=False, stack_axis=-1):
        """
        initialization steps after self.fluids is set.

        Subclasses should call _init_internal. after setting fluids.
        TODO (to make this less messy...):     Fluids should inherit
        from a class "FluidManip" which initializes from a list of Fluid
        objects...     And "FluidManip" should have __init__ ~=
        _init_internal.
        """
        self.numpy = numpy
        self.stack = stack
        self.stack_axis = stack_axis
        self._numpy_failed = None  # track last object for which conversion to numpy failed.
        self.SLs = [z for z in zip(self.spec, self.lvl)]
        self._SLdict = {SL: i for i, SL in enumerate(self.SLs)}  # dict for SL lookup.
        namedict = dict()
        for i, name in enumerate(self.name):
            if name in namedict.keys():  # handle duplicate name case:
                namedict = dict()  #    smash namedict; we will not use it if there are any duplicates.
                break  #    TODO (maybe): raise warning in this case?
            else:
                namedict[name.lower()] = i
        self._namedict = namedict  # dict for name lookup

    # list of attrs which should follow the usual __setattr__ logic,
    # rather than the overridden __setattr__ in this class:
    _INTERNAL_ATTRS = [
        "atoms",
        "files",
        "specs",
        "fluids",
        "SLs",
        "_SLdict",
        "_namedict",
        "_INTERNAL_ATTRS",
        "numpy",
        "_numpy_failed",
        "stack",
        "stack_axis",
        "assign_scalars",
        "assign_vectors",
        "_err",
    ]

    def neutrals(self):
        """
        return Fluids-like object with only neutral fluids.
        """
        return FluidsSlice(self, lambda fluid: fluid.ionization == 0)

    def ions(self):
        """
        return Fluids-like object with only ion fluids.
        """
        return FluidsSlice(self, lambda fluid: fluid.ionization >= 1)

    def where_attr_is(self, attr, value):
        """
        returns Fluids-like object with only fluids with fluid.attr == value.
        """
        return FluidsSlice(self, lambda fluid: (getattr(fluid, attr) == value))

    def setattr(self, attr, val):
        """
        sets self.__dict__[attr]=val.
        """
        self.__dict__[attr] = val

    def setcommon(self, attr, val):
        """sets fluid.attr = val for fluid in self.fluids.
        Unambiguous; always sets attr to val for each fluid.
        """
        for fluid in self.fluids:
            fluid.__setattr__(attr, val)

    @maintain_attrs("stack")
    def set_scalars(self, attr, scalars):
        """
        for fluid in self.fluids, sets fluid.attr to appropriate vals, for
        scalar case.

        scalars can be...:
            a single value --> for each fluid, set fluid.attr = scalar
            multiple values -> set fluids[i].attr = scalars[i].
                               requires len(scalars) >= len(fluids). Ignores extra values.
        """
        self.stack = False  # need this to be False while inputting. Will be restored after.
        self.__setattr__(attr, _shape_scalar(scalars, len(self)))

    @maintain_attrs("stack")
    def set_vectors(self, attr, vectors):
        """
        for fluid in self.fluids, sets fluid.attr to appropriate vals, for
        vector case.

        vectors can be...:
            a single vector --> for each fluid, set fluid.attr = vector
                                example: vectors=[1,2,3]
            multiple vectors -> set fluids[i].attr = vectors[i].
                                requires len(vectors) >= len(fluids). Ignores extra values.
                                example: vectors=[[1,2,3],[4,5,6]] (and len(fluids)<=2)
        """
        self.stack = False  # need this to be False while inputting. Will be restored after.
        self.__setattr__(attr, _shape_vector(vectors, len(self)))

    assign_scalars = set_scalars  # alias
    assign_vectors = set_vectors  # alias

    # ----------- define "magic" functions ----------- #
    def __iter__(self):  # allows to iterate over fluids (e.g. for fluid in fluids: ...)
        return iter(self.fluids)

    def __getitem__(self, i):  # allows to index fluids (e.g. fluids[0])
        """
        get fluid(s) i.

        i can be: int or slice ---> use normal indexing rules for the
        list self.fluids. an (S, L) pair -> return the fluid with
        species=S, level=L a fluid name ---> return the fluid with this
        name (e.g. 'He III'). Case insensitive.
        """
        try:
            iter(i)
        except TypeError:
            return self.fluids[i]
        else:
            # try to get idx from SLdict
            idx = self._SLdict.get(tuple(i), None)
            # if necessary, try to get idx from namedict
            if idx is None:
                if hasattr(i, "lower"):  # necessary for case insensitive lookup.
                    idx = self._namedict.get(i.lower(), None)
            # if got idx successfully, return the appropriate fluid.
            if idx is not None:
                return self.fluids[idx]
            # else, failed to get idx; raise error.
            else:
                MAX_NSHOW = 10  # max number of keys to show in error message.
                errmsg = (
                    "Could not find key={key:} in Fluids object.\n"
                    "key can be int, slice, (S, L) pair, or name of fluid.\n"
                    "The {firstSL:}available SL keys are:   {SLkeys:}.\n"
                    "The {firstname:}available name keys are: {namekeys:}. "
                    "(Name keys are case-insensitive for Fluids object indexing.)\n"
                    "For advanced debugging, see self._SLdict and self._namedict."
                )
                firstSL = "first {} ".format(MAX_NSHOW) if (MAX_NSHOW < len(self._SLdict)) else ""
                firstname = "first {} ".format(MAX_NSHOW) if (MAX_NSHOW < len(self._namedict)) else ""
                SLkeys = list(self._SLdict.keys())[:MAX_NSHOW]
                namekeys = list(self._namedict.keys())[:MAX_NSHOW]
                errmsg = errmsg.format(
                    key=repr(i),
                    firstSL=firstSL,
                    SLkeys=SLkeys,
                    firstname=firstname,
                    namekeys=namekeys,
                )
                raise IndexError(errmsg)

    def __len__(self):  # length of fluids (e.g. len(fluids))
        return len(self.fluids)

    def __repr__(self):
        return "<{} with SLs={}; atoms={}; fluids={}>".format(
            object.__repr__(self),
            self.SLs,
            _repr_list(self.atom),
            _repr_list(self.fluids),
        )

    def __str__(self):
        infotuples = [x for x in zip(self.element, self.ionization)]
        return "<{} with (specie, level)s={}; (element, charge)s={}>".format(
            object.__repr__(self), self.SLs, infotuples
        )

    def __getattr__(self, attr):  # only applies when self does not have attr.
        """
        return [value of attr for fluid in self.fluids] (unless attr is set in
        self).
        """
        x = [getattr(fluid, attr) for fluid in self.fluids]
        if self.numpy:
            try:
                if self.stack:
                    x = np.stack(x, axis=self.stack_axis)
                else:
                    x = np.array(x, copy=False)
            except Exception as err:
                self._err = err
                self._numpy_failed = x
        if len(x) < 1 or not callable(x[0]):
            return x
        else:  # x[0] is a function; return a function that outputs [x[i] applied to fluid i].
            return lambda *args, **kwargs: [f(*args, **kwargs) for f in x]

    def __setattr__(self, attr, val):
        """sets attr for each fluid in self.fluids:
        fluids[i].attr = val[i]    if len(val)==len(self.fluids)
        fluids[i].attr = val       otherwise.

        Note:
            if attr is a special attribute for the Fluids class,
            it will instead follow the usual setattr logic.
            The list of special attributes is at self._INTERNAL_ATTRS.
        call via "self.attr = val".
        """
        # handle INTERNAL_ATTRS and "dunder" methods (look like __methodname__)
        if attr in self._INTERNAL_ATTRS or (attr.startswith("__") and attr.endswith("__")):
            return self.setattr(attr, val)
        # figure out if we are setting val for every fluid, or getting i'th value for each fluid.
        globalval = None
        if self.numpy and self.stack:
            # handle stack=True case; we pull values from the stack_axis axis.
            val = np.array(val, copy=False)
            try:
                L = val.shape[self.stack_axis]
            except IndexError:
                globalval = True
            else:
                val_i = lambda i: np.take(val, i, axis=self.stack_axis)
        else:
            # handle stack=False case; we assign values in order from the input list.
            try:
                L = len(val)
            except TypeError:
                globalval = True
            else:
                val_i = lambda i: val[i]
        if globalval is None:
            globalval = L != len(self.fluids)
        # make function which tells how to set i'th value.
        if globalval:
            setval = lambda fluid, i: fluid.__setattr__(attr, val)
        else:
            setval = lambda fluid, i: fluid.__setattr__(attr, val_i(i))
        # loop through fluids; set i'th value for each fluid.
        for i, fluid in enumerate(self.fluids):
            setval(fluid, i)


class FluidsSlice(Fluids):
    """
    like Fluids class but init takes a Fluids object instead.
    """

    def __init__(self, fluids, keep):
        """
        keep fluids based on keep.

        keep can be slice, indices, or callable function which returns
        True/False.
        """
        self._INTERNAL_ATTRS += ["parent", "keep", "_keeptype", "idx"]
        self.parent = fluids
        self.keep = keep
        if isinstance(keep, slice):
            self._keeptype = "slice"
            self.idx = range(len(fluids))[keep]
        elif callable(keep):
            self._keeptype = "func"
            self.idx = [i for i, fluid in enumerate(fluids) if keep(fluid)]
        else:
            self._keeptype = "idx"
            self.idx = keep  # care, this doesn't create a copy of keep.
        self.fluids = [fluids[i] for i in self.idx]
        self._init_internal(numpy=fluids.numpy, stack=fluids.stack, stack_axis=fluids.stack_axis)


"""------------------------- Get atom files from EBYSUS/INPUT/ATOM_MS -------------------------"""


def _get_environ_EBYSUS_ATOMS_MS(ATOMS_MS=ATOMS_MS_ENVIRON):
    """
    if it doesn't already exist, sets os.environ['EBYSUS/RUNS'] to
    os.path.join(os.environ['EBYSUS'], RUNS).

    Then check if the path at os.environ['EBYSUS/RUNS'] actually exists - raise FileNotFoundError if it doesn't.
    returns os.environ['EBYSUS/RUNS']
    """
    ENV = "EBYSUS/ATOMS_MS"
    if ENV not in os.environ.keys():
        os.environ[ENV] = os.path.join(os.environ["EBYSUS"], ATOMS_MS)
    if not os.path.exists(os.environ[ENV]):
        raise FileNotFoundError(
            "os.environ['{:}'] == '{:}' does not exist!".format(ENV, os.environ[ENV])
            + " Probably you set the os.environ value incorrectly."
        )
    return os.environ[ENV]


def atomfiles(x=None, ATOMS_MS=ATOMS_MS_ENVIRON):
    """
    lists atomfiles available; searches current directory then ATOMS_MS folder.

    returns dict with keys "local", "remote", "remote_location", "order".

    x can be None or list of existing atomfiles. x[i] do not need to have '.atom' extension.
        x is None -> returns dict(local=list of existing local atomfiles,
                                  remote=list of existing atomfiles in ATOMS_MS,
                                  remote_location=ATOMS_MS,
                                  order=None).
        x is list -> returns dict(local=list of existing local atomfiles also found in x,
                                  remote=list of existing atomfiles in ATOMS_MS also found in x,
                                  remote_location=ATOMS_MS,
                                  order=[put_atom_extension_if_needed(f) for f in x]).
                     preserves order of enetered atoms.
    """

    def get_sorted_atoms(
        files,
    ):  # TODO: sort by element number instead of alphabetically. (implement this here.)
        return sorted([f for f in files if f.endswith(".atom")])

    local = get_sorted_atoms(os.listdir())
    remote_location = os.path.abspath(_get_environ_EBYSUS_ATOMS_MS(ATOMS_MS=ATOMS_MS))
    remote = get_sorted_atoms(os.listdir(remote_location))
    if x is None:
        return dict(local=local, remote=remote, remote_location=remote_location, order=None)
    else:  # assume x is a list
        _local = []
        _remote = []
        _x = []
        for f in x:
            # handle extension
            base, ext = os.path.splitext(f)
            if ext == ".atom" or ext == "":
                atomf = base + ".atom"
            else:
                raise ValueError(
                    "If extension is provided it must be '.atom', but got '{:}' for '{:}'".format(ext, f)
                )
            # check whether f is in local and/or remote.
            f_exists = False
            if atomf in local:
                _local += [atomf]
                f_exists = True
            if atomf in remote:
                _remote += [atomf]
                f_exists = True
            if not f_exists:
                raise FileNotFoundError(
                    "Couldn't find '{:}'; Files do not exist: '{:}'; '{:}'".format(
                        f,
                        *[os.path.abspath(os.path.join(place, f)) for place in [os.curdir, remote_location]],
                    )
                )
            _x += [atomf]
        return dict(local=_local, remote=_remote, remote_location=remote_location, order=_x)


def get_atomfiles(x=None, priority="local", ATOMS_MS=ATOMS_MS_ENVIRON):
    """
    single list of existing atomfiles; uses absolute filepaths.

    returns list of (absolute) filepaths to atomfiles.
    x = None, list, or dict
        None -> return list of all atomfiles, searching local directory then ATOMS_MS.
        list -> return list of abspaths (including filename) to all atom files in x.
                E.g. if 'H_2.atom' in local directory and 'He_3.atom' not in local directory,
                    get_atomfiles('H_2', 'He_3') returns ['local/H_2.atom', 'remote/He_3.atom']
                    (replacing local & remote with abspath to current dir and abspath to ATOMS_MS.)
                Files can be entered with or without '.atom' ending; e.g. 'H_2' and 'H_2.atom' are fine.
        dict -> assume x is like the output of atomfiles(). See atomfiles().
    priority = 'local' (default) or 'remote' (case-insensitive)
        in case of atom appearing in local and remote, whether to use local or remote file.
    """
    # priority validation & initial processing
    priority = priority.lower()
    VALID_P = ["local", "remote"]
    assert priority in VALID_P, "Expected priority in {:} but got '{:}'".format(VALID_P, priority)
    not_priority = "local" if priority == "remote" else "remote"
    # do atomfiles(x) if necessary.
    try:
        keys = x.keys()
    except Exception:
        x = atomfiles(x, ATOMS_MS=ATOMS_MS)
    else:
        needkeys = ("local", "remote", "remote_location", "order")
        assert set(keys) >= set(needkeys), "x needs keys {:} to be valid, but only had keys {:}".format(
            needkeys, keys
        )
    # convert to abspath based on place
    def abspathify(f, place):
        return os.path.abspath(f if place == "local" else (os.path.join(x["remote_location"], f)))

    # iterate and get results.
    result = []
    if x["order"] is None:
        for f in x[priority]:
            result += [abspathify(f, priority)]
        for f in x[not_priority]:
            result += [abspathify(f, not_priority)]
    else:  # x['order'] is not None
        for f in x["order"]:
            if f in x[priority]:
                result += [abspathify(f, priority)]
            elif f in x[not_priority]:
                result += [abspathify(f, not_priority)]
            else:
                assert (
                    False
                ), "f in x['order'] must be in x['local'] or x['remote'] but was not. f={:}, x={:}".format(
                    f, x
                )
    return result
